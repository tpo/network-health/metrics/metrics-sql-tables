CREATE TABLE IF NOT EXISTS bridgestrap_stats(
  bridgestrap_stats_end         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  interval                      BIGINT                       NOT NULL,
  digest                        TEXT,
  cached_requests               BIGINT,
  header                        TEXT,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS bridgestrap_test(
  digest                        TEXT                         NOT NULL,
  published                     TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  result                        BOOLEAN                      NOT NULL,
  fingerprint                   TEXT                         NOT NULL,
  bridgestrap_stats             TEXT references bridgestrap_stats(digest),
  PRIMARY KEY(digest)
);

CREATE index bridgestrap_test_published ON bridgestrap_test(published);
CREATE index bridgestrap_test_fingerprint ON bridgestrap_test(fingerprint);
CREATE index bridgestrap_test_fingerprint_published ON bridgestrap_test(fingerprint, published);
CREATE INDEX bridgestrap_test_fingerprint_published_desc_index ON bridgestrap_test (fingerprint, published DESC);
