-- Derived table from bandwidth_record, bandwidth_file,
-- bandwidth_file_document, server_status and server_descriptor
-- It's not created/updated by descriptorParser
CREATE TABLE IF NOT EXISTS bw_line(
  digest                                          TEXT,
  consensus_bandwidth                             BIGINT,
  desc_bw_avg                                     BIGINT,
  desc_bw_bur                                     BIGINT,
  desc_bw_obs_last                                BIGINT,
  nick                                            TEXT,
  node_id                                         TEXT,
  r_strm                                          DOUBLE PRECISION,
  r_strm_filt                                     DOUBLE PRECISION,
  relay_recent_measurements_excluded_error_count  INTEGER,
  stream_bw                                       BIGINT,
  success                                         INTEGER,
  time                                            TIMESTAMP WITHOUT TIME ZONE,
  vote                                            BOOLEAN,
  xoff_recv                                       INTEGER,
  xoff_sent                                       INTEGER,
  weight_bytes                                    INTEGER         NOT NULL,
  ratio                                           DOUBLE PRECISION,
  advertised_bw                                   BIGINT,
  fingerprint                                     TEXT,
  published                                       TIMESTAMP WITHOUT TIME ZONE,
  dirauth_nickname                                TEXT,
  scanner_country                                 TEXT,
  software_version                                TEXT,
  bandwidth_file_digest                           TEXT,
  advertised_bandwidth                            BIGINT,
  bandwidth_observed                              BIGINT,
  running                                         BOOLEAN
  -- not setting the PRIMARY KEY for now cause previously migrated data from
  -- the materialized view don't have a value for it
  -- PRIMARY KEY(digest)
);
-- digest can't be set to NOT NULL and PRIMARY KEY yet, cause there'se already
-- records without it and it hasn't been added via manual migrations.
CREATE UNIQUE INDEX IF NOT EXISTS bw_line_fingerprint_bandwidth_file_digest_idx ON bw_line(fingerprint, bandwidth_file_digest);
CREATE INDEX IF NOT EXISTS bw_line_dirauth_nickname_idx ON bw_line(dirauth_nickname);
CREATE INDEX IF NOT EXISTS bw_line_scanner_country_idx ON bw_line(scanner_country);
CREATE INDEX IF NOT EXISTS bw_line_fingerprint_idx ON bw_line(fingerprint);
CREATE INDEX IF NOT EXISTS bw_line_node_id_idx ON bw_line(node_id);
CREATE INDEX IF NOT EXISTS bw_line_published_idx ON bw_line(published);
CREATE INDEX IF NOT EXISTS bw_line_time_idx ON bw_line(time);
CREATE INDEX IF NOT EXISTS bw_line_stream_bw_idx ON bw_line(stream_bw);
CREATE INDEX IF NOT EXISTS bw_line_ratio_idx ON bw_line(ratio);
CREATE INDEX IF NOT EXISTS bw_line_weight_bytes_idx ON bw_line(weight_bytes);
CREATE INDEX IF NOT EXISTS bw_line_advertised_bw_idx ON bw_line(advertised_bw);
CREATE INDEX IF NOT EXISTS bw_line_fingeprint_weight_bytes_stream_bw_idx ON bw_line(fingerprint, weight_bytes, stream_bw);
CREATE INDEX IF NOT EXISTS bw_line_published_running_stream_bw_idx ON bw_line (published, running, stream_bw);


CREATE TABLE IF NOT EXISTS scanner_countries (
  scanner_country TEXT NOT NULL,
  PRIMARY KEY(scanner_country)
);
