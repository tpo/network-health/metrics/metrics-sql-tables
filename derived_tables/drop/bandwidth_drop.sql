-- CASCADE will also remove all the indexes, views and functions using the
-- tables.
DROP TABLE IF EXISTS bw_line CASCADE;
DROP TABLE IF EXISTS scanner_countries CASCADE;
