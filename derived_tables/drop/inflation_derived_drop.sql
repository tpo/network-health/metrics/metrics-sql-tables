-- The partitions by month will be also dropped when removing the main one
DROP TABLE IF EXISTS highest_advertised_bw_lowest_ratio_partitioned;
DROP TABLE IF EXISTS lowest_ratio_highest_advertised_bw_partitioned;
