-- Update data in inlation_derived tables

-- This should be automatized when we have more data
SELECT create_monthly_highest_advertised_bw_lowest_ratio_partitions('2024-09-01', '2024-12-31');

-- Automatize month creating when we have more data
-- Currently there aren't results for September and October
INSERT INTO highest_advertised_bw_lowest_ratio_partitioned (
    month_date,
    fingerprint,
    dirauth_nickname,
    scanner_country,
    ratio_avg,
    advertised_bandwidth_avg
)
SELECT DISTINCT
    '2024-12-01'::date,
    fingerprint,
    dirauth_nickname,
    scanner_country,
    ratio_avg,
    advertised_bandwidth_avg
FROM bw_avgs('2024-12-01', '2024-12-31')
WHERE ratio_avg > 0
-- filter out rows without observed bandwidth or observed bandwidth 0
AND advertised_bandwidth_avg > 0
ORDER BY advertised_bandwidth_avg DESC, ratio_avg
LIMIT 50;

SELECT create_monthly_lowest_ratio_highest_advertised_bw_partitions('2024-09-01', '2024-12-31');

INSERT INTO lowest_ratio_highest_advertised_bw_partitioned (
    month_date,
    fingerprint,
    dirauth_nickname,
    scanner_country,
    ratio_avg,
    advertised_bandwidth_avg
)
SELECT DISTINCT
    '2024-12-01'::date,
    fingerprint,
    dirauth_nickname,
    scanner_country,
    ratio_avg,
    advertised_bandwidth_avg
FROM bw_avgs('2024-12-01', '2024-12-31')
WHERE ratio_avg > 0
-- filter out rows without observed bandwidth or observed bandwidth 0
AND advertised_bandwidth_avg > 0
ORDER BY ratio_avg, advertised_bandwidth_avg DESC
LIMIT 50;
