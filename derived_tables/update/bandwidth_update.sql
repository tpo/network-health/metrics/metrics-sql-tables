-- Update derived table bw_line with external timer/cron, because it isn't
-- created/update by descriptorParser and no need to update hourly.

INSERT INTO bw_line (
  digest,
  stream_bw,
  consensus_bandwidth,
  desc_bw_avg,
  desc_bw_bur,
  desc_bw_obs_last,
  nick,
  node_id,
  r_strm,
  r_strm_filt,
  relay_recent_measurements_excluded_error_count,
  success,
  time,
  vote,
  xoff_sent,
  xoff_recv,
  weight_bytes,
  ratio,
  advertised_bw,
  fingerprint,
  published,
  dirauth_nickname,
  scanner_country,
  software_version,
  bandwidth_file_digest,
  advertised_bandwidth,
  bandwidth_observed,
  running
)
WITH brfp AS (
    SELECT
        CASE WHEN
            bandwidth_record.node_id LIKE '$%'
        THEN
            substring(bandwidth_record.node_id, 2)
        ELSE
            -- From sbws 2, node_id doesn't start with `$`
            bandwidth_record.node_id
        END AS fingerprint,
        bandwidth_record.digest
    FROM bandwidth_record
)
SELECT
  bandwidth_record.digest,
  bw_mean,
  consensus_bandwidth,
  desc_bw_avg,
  desc_bw_bur,
  desc_bw_obs_last,
  nick,
  node_id,
  r_strm,
  r_strm_filt,
  relay_recent_measurements_excluded_error_count,
  success,
  time,
  vote,
  xoff_sent,
  xoff_recv,
  bw as weight_bytes,
  greatest(bandwidth_record.r_strm, bandwidth_record.r_strm_filt) AS ratio,
  least(
        bandwidth_record.desc_bw_obs_last,
        bandwidth_record.desc_bw_avg,
        -- onionoo and descriptorParser calculate advertised bandwidth
        -- including burst, though not C tor
        bandwidth_record.desc_bw_bur
        ) AS advertised_bw,
  brfp.fingerprint,
  bandwidth_file.published,
  bandwidth_file.dirauth_nickname,
  bandwidth_file.scanner_country,
  bandwidth_file.software_version,
  bandwidth_file_document.bandwidth_file_digest,
  server_status.advertised_bandwidth,
  server_status.bandwidth_observed,
  server_status.running
FROM bandwidth_record
-- INNER JOIN is needed even though bandwidth_record contains published, etc
-- from bandwidth_file cause it contains only the first one.
INNER JOIN bandwidth_file_document
    ON bandwidth_file_document.bandwidth_record_digest = bandwidth_record.digest
INNER JOIN bandwidth_file
    ON bandwidth_file_document.bandwidth_file_digest = bandwidth_file.digest
INNER JOIN brfp
    ON brfp.digest = bandwidth_record.digest
LEFT JOIN server_status
    ON brfp.fingerprint = server_status.fingerprint
    AND date_trunc('hour', bandwidth_file.published) = server_status.published
WHERE bandwidth_file.published > (SELECT MAX(published) FROM bw_line);
