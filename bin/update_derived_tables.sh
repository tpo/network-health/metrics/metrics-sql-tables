#!/bin/bash

set -x

SCRIPT_PATH=$(dirname "${BASH_SOURCE[0]}")
PROJECT_PATH=$(dirname "$SCRIPT_PATH")
# No error if they're already created
for f in "$PROJECT_PATH"/derived_tables/create/*sql; do
    [ -e "$f" ] || continue
    psql -f "$f"
done

for f in "$PROJECT_PATH"/derived_tables/update/*sql; do
    [ -e "$f" ] || continue
    psql -f "$f"
done
