CREATE TABLE IF NOT EXISTS server_families(
  digest                              TEXT,
  effective_family                    TEXT,
  members                             INTEGER,
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  updated                             TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS server_status(
  is_bridge                           BOOLEAN                      NOT NULL,
  published                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  nickname                            TEXT                         NOT NULL,
  fingerprint                         TEXT                         NOT NULL,
  or_addresses                        TEXT,
  last_seen                           TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  first_seen                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  running                             BOOLEAN,
  flags                               TEXT,
  country                             TEXT,
  country_name                        TEXT,
  autonomous_system                   TEXT,
  as_name                             TEXT,
  verified_host_names                 TEXT,
  last_restarted                      TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  exit_policy                         TEXT,
  contact                             TEXT,
  platform                            TEXT,
  version                             TEXT,
  version_status                      TEXT,
  effective_family                    TEXT,
  declared_family                     TEXT,
  transport                           TEXT,
  bridgedb_distributor                TEXT,
  blocklist                           TEXT,
  last_changed_address_or_port        TIMESTAMP WITHOUT TIME ZONE,
  diff_or_addresses                   TEXT,
  unverified_host_names               TEXT,
  unreachable_or_addresses            TEXT,
  overload_ratelimits_version         INTEGER,
  overload_ratelimits_timestamp       TIMESTAMP WITHOUT TIME ZONE,
  overload_ratelimits_ratelimit       BIGINT,
  overload_ratelimits_burstlimit      BIGINT,
  overload_ratelimits_read_count      BIGINT,
  overload_ratelimits_write_count     BIGINT,
  overload_fd_exhausted_version       INTEGER,
  overload_fd_exhausted_timestamp     TIMESTAMP WITHOUT TIME ZONE,
  overload_general_timestamp          TIMESTAMP WITHOUT TIME ZONE,
  overload_general_version            INTEGER,
  first_network_status_digest         TEXT references network_status(digest),
  first_network_status_entry_digest   TEXT references network_status_entry(digest),
  last_network_status_digest          TEXT references network_status(digest),
  last_network_status_entry_digest    TEXT references network_status_entry(digest),
  first_bridge_network_status_digest         TEXT references bridge_network_status(digest),
  first_bridge_network_status_entry_digest   TEXT references bridge_network_status_entry(digest),
  last_bridge_network_status_digest          TEXT references bridge_network_status(digest),
  last_bridge_network_status_entry_digest    TEXT references bridge_network_status_entry(digest),
  last_network_status_weights_id      uuid references network_status_entry_weights(weights_id),
  last_network_status_totals_id       uuid references network_status_totals(totals_id),
  last_server_descriptor_digest       TEXT references server_descriptor(digest_sha1_hex),
  last_extrainfo_descriptor_digest    TEXT references extra_info_descriptor(digest_sha1_hex),
  last_bridgestrap_stats_digest       TEXT references bridgestrap_stats(digest),
  last_bridgestrap_test_digest        TEXT references bridgestrap_test(digest),
  last_bridge_pool_assignments_file_digest  TEXT references bridge_pool_assignments_file(digest),
  last_bridge_pool_assignment_digest  TEXT references bridge_pool_assignment(digest),
  family_digest                       TEXT references server_families(digest),
  advertised_bandwidth                BIGINT,
  bandwidth                           BIGINT,
  network_weight_fraction             FLOAT,
  bandwidth_avg                       BIGINT,
  bandwidth_burst                     BIGINT,
  bandwidth_observed                  BIGINT,
  ipv6_default_policy                 TEXT,
  ipv6_port_list                      TEXT,
  socks_port                          INTEGER,
  dir_port                            INTEGER,
  or_port                             INTEGER,
  is_hibernating                      BOOLEAN,
  address                             TEXT,
  is_stale                            BOOLEAN,
  PRIMARY KEY(fingerprint, nickname, published)
);

CREATE TABLE IF NOT EXISTS extra_info_bandwidth_history (
  published                         TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  nickname                          TEXT                         NOT NULL,
  fingerprint                       TEXT                         NOT NULL,
  flags                             TEXT,
  time                              TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  write_bandwidth_value             BIGINT,
  read_bandwidth_value              BIGINT,
  ipv6_write_bandwidth_value        BIGINT,
  ipv6_read_bandwidth_value         BIGINT,
  dirreq_write_bandwidth_value      BIGINT,
  dirreq_read_bandwidth_value       BIGINT,
  extra_info_digest                 TEXT,  

  -- Ensure the combination of nickname, fingerprint, and time is unique
  CONSTRAINT unique_bw_entry UNIQUE (nickname, fingerprint, time)
);

CREATE index server_status_published ON server_status(published);
CREATE index server_status_fingerprint ON server_status(fingerprint);
CREATE index server_status_nickname ON server_status(nickname);
CREATE index server_status_is_bridge ON server_status(is_bridge);
CREATE index server_status_running ON server_status(running);

CREATE INDEX server_status_optimized_index
ON server_status (fingerprint, nickname, published DESC);

CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX server_families_effective_family_trgm_idx ON server_families USING gin (effective_family gin_trgm_ops);

CREATE INDEX server_status_fingerprint_nickname_published_dirauth_idx ON server_status(fingerprint, nickname, published DESC) 
WHERE is_bridge = False AND flags ~ 'Authority';

CREATE INDEX server_status_family_fingerprint_nickname_published_idx
ON server_status (family_digest, fingerprint, nickname, published DESC);

CREATE INDEX server_status_flags_idx ON server_status (flags);
CREATE INDEX server_status_published_flags_idx ON server_status (published, flags);

CREATE MATERIALIZED VIEW server_advertised_bandwidth_by_day AS
WITH unique_fingerprints AS (
    SELECT
        fingerprint,
        DATE(published) AS day,
        advertised_bandwidth,
        flags
    FROM
        server_status
    GROUP BY
        fingerprint, DATE(published), advertised_bandwidth, flags
        HAVING DATE(published) = MAX(DATE(published))
)
SELECT
    day,

    -- Total advertised bandwidth for "Guard Only" relays
    SUM(CASE 
        WHEN flags LIKE '%Guard%' AND flags NOT LIKE '%Exit%' 
        THEN advertised_bandwidth 
        ELSE 0 
    END) AS total_advertised_bandwidth_guard_only,

    -- Total advertised bandwidth for "Exit Only" relays
    SUM(CASE 
        WHEN flags LIKE '%Exit%' AND flags NOT LIKE '%Guard%' 
        THEN advertised_bandwidth 
        ELSE 0 
    END) AS total_advertised_bandwidth_exit_only,

    -- Total advertised bandwidth for relays that are both "Guard" and "Exit"
    SUM(CASE 
        WHEN flags LIKE '%Guard%' AND flags LIKE '%Exit%' 
        THEN advertised_bandwidth 
        ELSE 0 
    END) AS total_advertised_bandwidth_guard_and_exit

FROM
    unique_fingerprints
GROUP BY
    day
ORDER BY
    day;
