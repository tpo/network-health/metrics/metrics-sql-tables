-- List functions
-- ==============

-- Return all function that aren't in the namespaces
-- `information_schema`, `pg_catalog` aren't in `c` language
SELECT p.proname AS function_name
FROM pg_proc AS p
LEFT JOIN pg_namespace AS n ON p.pronamespace = n.oid
LEFT JOIN pg_language AS l ON p.prolang = l.oid
WHERE
    n.nspname NOT IN ('pg_catalog', 'information_schema')
    AND l.lanname != 'c'  -- sql or plpgsql
ORDER BY function_name;
