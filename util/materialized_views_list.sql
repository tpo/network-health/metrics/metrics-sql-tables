-- List materialized views
-- =======================

SELECT matviewname AS view_name,
FROM pg_matviews
ORDER BY view_name;
