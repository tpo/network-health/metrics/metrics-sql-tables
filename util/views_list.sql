-- List views
-- ==========

-- Show (non materialized) views. Do not include views from system.
SELECT table_name
FROM information_schema.views
WHERE table_schema NOT IN ('information_schema', 'pg_catalog')
ORDER BY table_name;
