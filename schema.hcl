table "bandwidth_file" {
  schema = schema.public
  column "header" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "destination_countries" {
    null = false
    type = text
  }
  column "earliest_bandwidth" {
    null = false
    type = timestamp
  }
  column "file_created" {
    null = false
    type = timestamp
  }
  column "generator_started" {
    null = false
    type = timestamp
  }
  column "latest_bandwidth" {
    null = false
    type = timestamp
  }
  column "minimum_number_eligible_relays" {
    null = true
    type = integer
  }
  column "minimum_percent_eligible_relays" {
    null = true
    type = integer
  }
  column "number_consensus_relays" {
    null = true
    type = integer
  }
  column "number_eligible_relays" {
    null = true
    type = integer
  }
  column "percent_eligible_relays" {
    null = true
    type = integer
  }
  column "recent_consensus_count" {
    null = true
    type = integer
  }
  column "recent_measurement_attempt_count" {
    null = true
    type = integer
  }
  column "recent_measurement_failure_count" {
    null = true
    type = integer
  }
  column "recent_measurements_excluded_error_count" {
    null = true
    type = integer
  }
  column "recent_measurements_excluded_few_count" {
    null = true
    type = integer
  }
  column "recent_measurements_excluded_near_count" {
    null = true
    type = integer
  }
  column "recent_measurements_excluded_old_count" {
    null = true
    type = integer
  }
  column "recent_priority_list_count" {
    null = true
    type = integer
  }
  column "recent_priority_relay_count" {
    null = true
    type = integer
  }
  column "scanner_country" {
    null = true
    type = text
  }
  column "software" {
    null = true
    type = text
  }
  column "software_version" {
    null = true
    type = text
  }
  column "tor_version" {
    null = true
    type = text
  }
  column "time_to_report_half_network" {
    null = true
    type = integer
  }
  column "spec_version" {
    null = true
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "mu" {
    null = true
    type = integer
  }
  column "muf" {
    null = true
    type = integer
  }
  column "dirauth_nickname" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "bandwidth_file_published_idx" {
    columns = [column.published]
  }
  index "bandwidth_file_scanner_country_idx" {
    columns = [column.scanner_country]
  }
}
table "bandwidth_file_document" {
  schema = schema.public
  column "bandwidth_file_digest" {
    null = false
    type = text
  }
  column "bandwidth_record_digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.bandwidth_record_digest, column.bandwidth_file_digest]
  }
  foreign_key "bandwidth_file_document_bandwidth_file_digest_fkey" {
    columns     = [column.bandwidth_file_digest]
    ref_columns = [table.bandwidth_file.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "bandwidth_file_document_bandwidth_record_digest_fkey" {
    columns     = [column.bandwidth_record_digest]
    ref_columns = [table.bandwidth_record.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "bandwidth_record" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "bw" {
    null = true
    type = integer
  }
  column "bw_mean" {
    null = true
    type = bigint
  }
  column "bw_median" {
    null = true
    type = bigint
  }
  column "consensus_bandwidth" {
    null = true
    type = bigint
  }
  column "consensus_bandwidth_is_unmeasured" {
    null = true
    type = boolean
  }
  column "desc_bw_avg" {
    null = true
    type = bigint
  }
  column "desc_bw_bur" {
    null = true
    type = bigint
  }
  column "desc_bw_obs_last" {
    null = true
    type = bigint
  }
  column "desc_bw_obs_mean" {
    null = true
    type = bigint
  }
  column "error_circ" {
    null = true
    type = integer
  }
  column "error_destination" {
    null = true
    type = integer
  }
  column "error_misc" {
    null = true
    type = integer
  }
  column "error_second_relay" {
    null = true
    type = integer
  }
  column "error_stream" {
    null = true
    type = integer
  }
  column "master_key_ed25519" {
    null = true
    type = text
  }
  column "nick" {
    null = true
    type = text
  }
  column "node_id" {
    null = true
    type = text
  }
  column "rtt" {
    null = true
    type = integer
  }
  column "relay_in_recent_consensus_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurement_attempt_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurements_excluded_error_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurement_failure_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurements_excluded_near_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurements_excluded_old_count" {
    null = true
    type = integer
  }
  column "relay_recent_measurements_excluded_few_count" {
    null = true
    type = integer
  }
  column "relay_recent_priority_list_count" {
    null = true
    type = integer
  }
  column "under_min_report" {
    null = true
    type = boolean
  }
  column "unmeasured" {
    null = true
    type = boolean
  }
  column "vote" {
    null = true
    type = boolean
  }
  column "xoff_recv" {
    null = true
    type = integer
  }
  column "xoff_sent" {
    null = true
    type = integer
  }
  column "success" {
    null = true
    type = integer
  }
  column "time" {
    null = false
    type = timestamp
  }
  column "r_strm" {
    null = true
    type = double_precision
  }
  column "r_strm_filt" {
    null = true
    type = double_precision
  }
  column "dirauth_nickname" {
    null = true
    type = text
  }
  column "scanner_country" {
    null = true
    type = text
  }
  column "published" {
    null = true
    type = timestamp
  }
  column "software_version" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "bandwdith_record_time_idx" {
    columns = [column.time]
  }
  index "bandwidth_record_published_idx" {
    columns = [column.published]
  }
}
table "bridge_network_status" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "flag_thresholds" {
    null = true
    type = text
  }
  column "stable_uptime" {
    null = true
    type = bigint
  }
  column "stable_mtbf" {
    null = true
    type = bigint
  }
  column "fast_bandwidth" {
    null = true
    type = bigint
  }
  column "guard_wfu" {
    null = true
    type = double_precision
  }
  column "guard_tk" {
    null = true
    type = bigint
  }
  column "guard_bandwidth_including_exits" {
    null = true
    type = bigint
  }
  column "guard_bandwidth_excluding_exits" {
    null = true
    type = bigint
  }
  column "enough_mtbf_info" {
    null = true
    type = integer
  }
  column "ignore_adv_bws" {
    null = true
    type = integer
  }
  column "header" {
    null = true
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "bridge_network_status_published_idx" {
    columns = [column.published]
  }
}
table "bridge_network_status_document" {
  schema = schema.public
  column "bridge_network_status_digest" {
    null = true
    type = text
  }
  column "bridge_network_status_entry_digest" {
    null = true
    type = text
  }
  foreign_key "bridge_network_status_documen_bridge_network_status_digest_fkey" {
    columns     = [column.bridge_network_status_digest]
    ref_columns = [table.bridge_network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "bridge_network_status_documen_bridge_network_status_entry__fkey" {
    columns     = [column.bridge_network_status_entry_digest]
    ref_columns = [table.bridge_network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "bridge_network_status_entry" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "nickname" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "network_status" {
    null = true
    type = text
  }
  column "address" {
    null = true
    type = text
  }
  column "or_port" {
    null = true
    type = integer
  }
  column "dir_port" {
    null = true
    type = integer
  }
  column "or_address" {
    null = true
    type = text
  }
  column "flags" {
    null = true
    type = text
  }
  column "bandwidth" {
    null = true
    type = bigint
  }
  column "policy" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "bridge_network_status_entry_network_status_fkey" {
    columns     = [column.network_status]
    ref_columns = [table.bridge_network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "bridge_network_status_entry_fingerprint" {
    columns = [column.fingerprint]
  }
  index "bridge_network_status_entry_nickname" {
    columns = [column.nickname]
  }
  index "bridge_network_status_entry_published" {
    columns = [column.published]
  }
  index "bridge_status_fingerprint_nickname_published_asc_index" {
    columns = [column.fingerprint, column.nickname, column.published]
  }
  index "bridge_status_fingerprint_nickname_published_desc_index" {
    on {
      column = column.fingerprint
    }
    on {
      column = column.nickname
    }
    on {
      desc   = true
      column = column.published
    }
  }
}
table "bridge_pool_assignment" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "digest" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "distribution_method" {
    null = false
    type = text
  }
  column "transport" {
    null = true
    type = text
  }
  column "ip" {
    null = true
    type = text
  }
  column "blocklist" {
    null = true
    type = text
  }
  column "bridge_pool_assignments" {
    null = true
    type = text
  }
  column "distributed" {
    null = true
    type = boolean
  }
  column "state" {
    null = true
    type = text
  }
  column "bandwidth" {
    null = true
    type = text
  }
  column "ratio" {
    null = true
    type = real
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "bridge_pool_assignment_bridge_pool_assignments_fkey" {
    columns     = [column.bridge_pool_assignments]
    ref_columns = [table.bridge_pool_assignments_file.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "bridge_pool_assignment_fingerprint" {
    columns = [column.fingerprint]
  }
  index "bridge_pool_assignment_fingerprint_published_desc_index" {
    on {
      column = column.fingerprint
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "bridge_pool_assignment_published" {
    columns = [column.published]
  }
}
table "bridge_pool_assignments_file" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "header" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "bridge_pool_assignment_file_published" {
    columns = [column.published]
  }
}
table "bridgedb_metrics" {
  schema = schema.public
  column "bridgedb_metrics_end" {
    null = false
    type = timestamp
  }
  column "interval" {
    null = false
    type = bigint
  }
  column "digest" {
    null = false
    type = text
  }
  column "version" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
}
table "bridgedb_metrics_count" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "time" {
    null = false
    type = timestamp
  }
  column "distribution" {
    null = true
    type = text
  }
  column "transport" {
    null = true
    type = text
  }
  column "country" {
    null = true
    type = text
  }
  column "status" {
    null = true
    type = text
  }
  column "tests" {
    null = true
    type = text
  }
  column "value" {
    null = true
    type = bigint
  }
  column "metrics" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest, column.time]
  }
  foreign_key "bridgedb_metrics_count_metrics_fkey" {
    columns     = [column.metrics]
    ref_columns = [table.bridgedb_metrics.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "bridgestrap_stats" {
  schema = schema.public
  column "bridgestrap_stats_end" {
    null = false
    type = timestamp
  }
  column "interval" {
    null = false
    type = bigint
  }
  column "digest" {
    null = false
    type = text
  }
  column "cached_requests" {
    null = true
    type = bigint
  }
  column "header" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
}
table "bridgestrap_test" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "result" {
    null = false
    type = boolean
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "bridgestrap_stats" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "bridgestrap_test_bridgestrap_stats_fkey" {
    columns     = [column.bridgestrap_stats]
    ref_columns = [table.bridgestrap_stats.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "bridgestrap_test_fingerprint" {
    columns = [column.fingerprint]
  }
  index "bridgestrap_test_fingerprint_published" {
    columns = [column.fingerprint, column.published]
  }
  index "bridgestrap_test_fingerprint_published_desc_index" {
    on {
      column = column.fingerprint
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "bridgestrap_test_published" {
    columns = [column.published]
  }
}
table "bw_line" {
  schema = schema.public
  column "digest" {
    null = true
    type = text
  }
  column "consensus_bandwidth" {
    null = true
    type = bigint
  }
  column "desc_bw_avg" {
    null = true
    type = bigint
  }
  column "desc_bw_bur" {
    null = true
    type = bigint
  }
  column "desc_bw_obs_last" {
    null = true
    type = bigint
  }
  column "nick" {
    null = true
    type = text
  }
  column "node_id" {
    null = true
    type = text
  }
  column "r_strm" {
    null = true
    type = double_precision
  }
  column "r_strm_filt" {
    null = true
    type = double_precision
  }
  column "relay_recent_measurements_excluded_error_count" {
    null = true
    type = integer
  }
  column "stream_bw" {
    null = true
    type = bigint
  }
  column "success" {
    null = true
    type = integer
  }
  column "time" {
    null = true
    type = timestamp
  }
  column "vote" {
    null = true
    type = boolean
  }
  column "xoff_recv" {
    null = true
    type = integer
  }
  column "xoff_sent" {
    null = true
    type = integer
  }
  column "weight_bytes" {
    null = false
    type = integer
  }
  column "ratio" {
    null = true
    type = double_precision
  }
  column "advertised_bw" {
    null = true
    type = bigint
  }
  column "fingerprint" {
    null = true
    type = text
  }
  column "published" {
    null = true
    type = timestamp
  }
  column "dirauth_nickname" {
    null = true
    type = text
  }
  column "scanner_country" {
    null = true
    type = text
  }
  column "software_version" {
    null = true
    type = text
  }
  column "bandwidth_file_digest" {
    null = true
    type = text
  }
  column "advertised_bandwidth" {
    null = true
    type = bigint
  }
  column "bandwidth_observed" {
    null = true
    type = bigint
  }
  column "running" {
    null = true
    type = boolean
  }
  index "bw_line_advertised_bw_idx" {
    columns = [column.advertised_bw]
  }
  index "bw_line_dirauth_nickname_idx" {
    columns = [column.dirauth_nickname]
  }
  index "bw_line_fingeprint_weight_bytes_stream_bw_idx" {
    columns = [column.fingerprint, column.weight_bytes, column.stream_bw]
  }
  index "bw_line_fingerprint_bandwidth_file_digest_idx" {
    unique  = true
    columns = [column.fingerprint, column.bandwidth_file_digest]
  }
  index "bw_line_fingerprint_idx" {
    columns = [column.fingerprint]
  }
  index "bw_line_node_id_idx" {
    columns = [column.node_id]
  }
  index "bw_line_published_idx" {
    columns = [column.published]
  }
  index "bw_line_published_running_stream_bw_idx" {
    columns = [column.published, column.running, column.stream_bw]
  }
  index "bw_line_ratio_idx" {
    columns = [column.ratio]
  }
  index "bw_line_scanner_country_idx" {
    columns = [column.scanner_country]
  }
  index "bw_line_stream_bw_idx" {
    columns = [column.stream_bw]
  }
  index "bw_line_time_idx" {
    columns = [column.time]
  }
  index "bw_line_weight_bytes_idx" {
    columns = [column.weight_bytes]
  }
}
table "exit_list" {
  schema = schema.public
  column "downloaded" {
    null = false
    type = timestamp
  }
  column "header" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "exit_list_downloaded" {
    columns = [column.downloaded]
  }
}
table "exit_node" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "last_status" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "exit_addresses" {
    null = false
    type = text
  }
  column "exit_addresses_timestamps" {
    null = false
    type = text
  }
  column "exit_list" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "exit_node_exit_list_fkey" {
    columns     = [column.exit_list]
    ref_columns = [table.exit_list.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "exit_node_published" {
    columns = [column.published]
  }
}
table "extra_info_bandwidth_history" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "flags" {
    null = true
    type = text
  }
  column "time" {
    null = false
    type = timestamp
  }
  column "write_bandwidth_value" {
    null = true
    type = bigint
  }
  column "read_bandwidth_value" {
    null = true
    type = bigint
  }
  column "ipv6_write_bandwidth_value" {
    null = true
    type = bigint
  }
  column "ipv6_read_bandwidth_value" {
    null = true
    type = bigint
  }
  column "dirreq_write_bandwidth_value" {
    null = true
    type = bigint
  }
  column "dirreq_read_bandwidth_value" {
    null = true
    type = bigint
  }
  column "extra_info_digest" {
    null = true
    type = text
  }
  unique "unique_bw_entry" {
    columns = [column.nickname, column.fingerprint, column.time]
  }
}
table "extra_info_descriptor" {
  schema = schema.public
  column "is_bridge" {
    null = false
    type = boolean
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "digest_sha1_hex" {
    null = false
    type = text
  }
  column "digest_sha256" {
    null = false
    type = text
  }
  column "identity_ed25519" {
    null = true
    type = text
  }
  column "master_key_ed25519" {
    null = true
    type = text
  }
  column "read_history" {
    null = true
    type = text
  }
  column "write_history" {
    null = true
    type = text
  }
  column "ipv6_read_history" {
    null = true
    type = text
  }
  column "ipv6_write_history" {
    null = true
    type = text
  }
  column "geoip_db_digest" {
    null = true
    type = text
  }
  column "geoip6_db_digest" {
    null = true
    type = text
  }
  column "geoip_start_time" {
    null = true
    type = timestamp
  }
  column "geoip_client_origins" {
    null = true
    type = text
  }
  column "bridge_stats_end" {
    null = true
    type = timestamp
  }
  column "bridge_stats_inverval" {
    null = true
    type = bigint
  }
  column "bridge_ips" {
    null = true
    type = text
  }
  column "bridge_ip_versions" {
    null = true
    type = text
  }
  column "bridge_ip_transports" {
    null = true
    type = text
  }
  column "dirreq_stats_end" {
    null = true
    type = timestamp
  }
  column "dirreq_stats_interval" {
    null = true
    type = bigint
  }
  column "dirreq_v2_ips" {
    null = true
    type = text
  }
  column "dirreq_v3_ips" {
    null = true
    type = text
  }
  column "dirreq_v2_reqs" {
    null = true
    type = text
  }
  column "dirreq_v3_reqs" {
    null = true
    type = text
  }
  column "dirreq_v2_share" {
    null = true
    type = double_precision
  }
  column "dirreq_v3_share" {
    null = true
    type = double_precision
  }
  column "dirreq_v2_resp" {
    null = true
    type = text
  }
  column "dirreq_v3_resp" {
    null = true
    type = text
  }
  column "dirreq_v2_direct_dl" {
    null = true
    type = text
  }
  column "dirreq_v3_direct_dl" {
    null = true
    type = text
  }
  column "dirreq_v2_tunneled_dl" {
    null = true
    type = text
  }
  column "dirreq_v3_tunneled_dl" {
    null = true
    type = text
  }
  column "dirreq_read_history" {
    null = true
    type = text
  }
  column "dirreq_write_history" {
    null = true
    type = text
  }
  column "entry_stats_end" {
    null = true
    type = timestamp
  }
  column "entry_stats_interval" {
    null = true
    type = bigint
  }
  column "entry_ips" {
    null = true
    type = text
  }
  column "cell_stats_end" {
    null = true
    type = timestamp
  }
  column "cell_stats_interval" {
    null = true
    type = bigint
  }
  column "cell_processed_cells" {
    null = true
    type = sql("integer[]")
  }
  column "cell_queued_cells" {
    null = true
    type = sql("double precision[]")
  }
  column "cell_time_in_queue" {
    null = true
    type = sql("double precision[]")
  }
  column "cell_circuits_per_decile" {
    null = true
    type = bigint
  }
  column "conn_bi_direct_timestamp" {
    null = true
    type = timestamp
  }
  column "conn_bi_direct_interval" {
    null = true
    type = bigint
  }
  column "conn_bi_direct_below" {
    null = true
    type = bigint
  }
  column "conn_bi_direct_read" {
    null = true
    type = bigint
  }
  column "conn_bi_direct_write" {
    null = true
    type = bigint
  }
  column "conn_bi_direct_both" {
    null = true
    type = bigint
  }
  column "ipv6_conn_bi_direct_timestamp" {
    null = true
    type = timestamp
  }
  column "ipv6_conn_bi_direct_interval" {
    null = true
    type = bigint
  }
  column "ipv6_conn_bi_direct_below" {
    null = true
    type = bigint
  }
  column "ipv6_conn_bi_direct_read" {
    null = true
    type = bigint
  }
  column "ipv6_conn_bi_direct_write" {
    null = true
    type = bigint
  }
  column "ipv6_conn_bi_direct_both" {
    null = true
    type = bigint
  }
  column "exit_stats_end" {
    null = true
    type = timestamp
  }
  column "exit_stats_inverval" {
    null = true
    type = bigint
  }
  column "exit_kibibytes_written" {
    null = true
    type = text
  }
  column "exit_kibibytes_read" {
    null = true
    type = text
  }
  column "exit_streams_opened" {
    null = true
    type = text
  }
  column "hidserv_stats_end" {
    null = true
    type = timestamp
  }
  column "hidserv_stats_interval" {
    null = true
    type = bigint
  }
  column "hidserv_v3_stats_end" {
    null = true
    type = timestamp
  }
  column "hidserv_v3_stats_interval" {
    null = true
    type = bigint
  }
  column "hidserv_rend_relayed_cells_value" {
    null = true
    type = numeric
  }
  column "hidserv_rend_relayed_cells" {
    null = true
    type = text
  }
  column "hidserv_rend_v3_relayed_cells_value" {
    null = true
    type = numeric
  }
  column "hidserv_rend_v3_relayed_cells" {
    null = true
    type = text
  }
  column "hidserv_dir_onions_seen_value" {
    null = true
    type = numeric
  }
  column "hidserv_dir_onions_seen" {
    null = true
    type = text
  }
  column "hidserv_dir_v3_onions_seen_value" {
    null = true
    type = numeric
  }
  column "hidserv_dir_v3_onions_seen" {
    null = true
    type = text
  }
  column "transports" {
    null = true
    type = text
  }
  column "padding_counts_timestamp" {
    null = true
    type = timestamp
  }
  column "padding_counts_interval" {
    null = true
    type = bigint
  }
  column "padding_counts" {
    null = true
    type = text
  }
  column "overload_ratelimits_version" {
    null = true
    type = integer
  }
  column "overload_ratelimits_timestamp" {
    null = true
    type = timestamp
  }
  column "overload_ratelimits_ratelimit" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_burstlimit" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_read_count" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_write_count" {
    null = true
    type = bigint
  }
  column "overload_fd_exhausted_version" {
    null = true
    type = integer
  }
  column "overload_fd_exhausted_timestamp" {
    null = true
    type = timestamp
  }
  column "router_sig_ed25519" {
    null = true
    type = text
  }
  column "router_signature" {
    null = true
    type = text
  }
  column "header" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest_sha1_hex]
  }
  index "extra_info_descriptor_published" {
    columns = [column.published]
  }
}
table "family_tags" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "tags" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "edited" {
    null = false
    type = timestamp
  }
  column "status" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
}
table "log_line" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "log_digest" {
    null = true
    type = text
  }
  column "ip" {
    null = true
    type = text
  }
  column "method" {
    null = true
    type = text
  }
  column "protocol" {
    null = true
    type = text
  }
  column "request" {
    null = true
    type = text
  }
  column "size" {
    null = true
    type = bigint
  }
  column "response" {
    null = true
    type = integer
  }
  column "is_valid" {
    null = true
    type = boolean
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "log_line_log_digest_fkey" {
    columns     = [column.log_digest]
    ref_columns = [table.server_log.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "microdescriptor" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "time" {
    null = false
    type = timestamp
  }
  column "or_addresses" {
    null = false
    type = text
  }
  column "policy" {
    null = true
    type = text
  }
  column "port_list" {
    null = true
    type = text
  }
  column "onion_key" {
    null = true
    type = text
  }
  column "ntor_onion_key" {
    null = true
    type = text
  }
  column "family_entries" {
    null = true
    type = text
  }
  column "ipv6_policy" {
    null = true
    type = text
  }
  column "ipv6_port_list" {
    null = true
    type = text
  }
  column "rsa_identity" {
    null = true
    type = text
  }
  column "ed25519_identity" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "microdescriptor_time" {
    columns = [column.time]
  }
}
table "network_status" {
  schema = schema.public
  column "header" {
    null = false
    type = text
  }
  column "network_status_version" {
    null = false
    type = integer
  }
  column "vote_status" {
    null = true
    type = text
  }
  column "consensus_method" {
    null = true
    type = integer
  }
  column "consensus_flavor" {
    null = false
    type = text
  }
  column "valid_after" {
    null = true
    type = timestamp
  }
  column "fresh_until" {
    null = true
    type = timestamp
  }
  column "valid_until" {
    null = true
    type = timestamp
  }
  column "vote_seconds" {
    null = true
    type = bigint
  }
  column "dist_seconds" {
    null = true
    type = bigint
  }
  column "known_flags" {
    null = false
    type = text
  }
  column "recommended_client_versions" {
    null = true
    type = text
  }
  column "recommended_server_versions" {
    null = true
    type = text
  }
  column "recommended_client_protocols" {
    null = false
    type = text
  }
  column "recommended_relay_protocols" {
    null = false
    type = text
  }
  column "required_client_protocols" {
    null = false
    type = text
  }
  column "required_relay_protocols" {
    null = false
    type = text
  }
  column "params" {
    null = false
    type = text
  }
  column "package_lines" {
    null = false
    type = text
  }
  column "shared_rand_previous_value" {
    null = true
    type = text
  }
  column "shared_rand_current_value" {
    null = true
    type = text
  }
  column "shared_rand_previous_num" {
    null = true
    type = text
  }
  column "shared_rand_current_num" {
    null = true
    type = text
  }
  column "dir_sources" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "bandwidth_weights" {
    null = false
    type = text
  }
  column "directory_signatures" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "network_status_valid_after_desc_index" {
    on {
      desc   = true
      column = column.valid_after
    }
  }
  index "network_status_valid_after_idx" {
    columns = [column.valid_after]
  }
}
table "network_status_document" {
  schema = schema.public
  column "network_status_digest" {
    null = true
    type = text
  }
  column "network_status_entry_digest" {
    null = true
    type = text
  }
  foreign_key "network_status_document_network_status_digest_fkey" {
    columns     = [column.network_status_digest]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "network_status_document_network_status_entry_digest_fkey" {
    columns     = [column.network_status_entry_digest]
    ref_columns = [table.network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "network_status_entry" {
  schema = schema.public
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "ip" {
    null = false
    type = text
  }
  column "or_port" {
    null = false
    type = integer
  }
  column "dir_port" {
    null = false
    type = integer
  }
  column "or_addresses" {
    null = false
    type = text
  }
  column "flags" {
    null = true
    type = text
  }
  column "version" {
    null = true
    type = text
  }
  column "bandwidth_unmeasured" {
    null = true
    type = boolean
  }
  column "bandwidth_weight" {
    null = true
    type = bigint
  }
  column "proto" {
    null = true
    type = text
  }
  column "policy" {
    null = true
    type = text
  }
  column "port_list" {
    null = true
    type = text
  }
  column "flavor" {
    null = true
    type = text
  }
  column "microdescriptor_digest" {
    null = true
    type = text
  }
  column "stats" {
    null = true
    type = text
  }
  column "master_key_ed25519" {
    null = true
    type = text
  }
  column "network_status" {
    null = true
    type = text
  }
  column "server_descriptor_digest" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "network_status_entry_network_status_fkey" {
    columns     = [column.network_status]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "network_status_entry_asc_index" {
    columns = [column.fingerprint, column.nickname, column.published]
  }
  index "network_status_entry_desc_index" {
    on {
      column = column.fingerprint
    }
    on {
      column = column.nickname
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "network_status_entry_fingerprint_idx" {
    columns = [column.fingerprint]
  }
  index "network_status_entry_fingerprint_nickname_published_idx" {
    columns = [column.fingerprint, column.nickname, column.published]
  }
  index "network_status_entry_network_status_idx" {
    columns = [column.network_status]
  }
  index "network_status_entry_nickname_idx" {
    columns = [column.nickname]
  }
  index "network_status_entry_published_idx" {
    columns = [column.published]
  }
}
table "network_status_entry_weights" {
  schema = schema.public
  column "weights_id" {
    null    = false
    type    = uuid
    default = sql("uuid_generate_v4()")
  }
  column "guard_weight" {
    null = true
    type = double_precision
  }
  column "middle_weight" {
    null = true
    type = double_precision
  }
  column "exit_weight" {
    null = true
    type = double_precision
  }
  column "guard_weight_fraction" {
    null = true
    type = double_precision
  }
  column "middle_weight_fraction" {
    null = true
    type = double_precision
  }
  column "exit_weight_fraction" {
    null = true
    type = double_precision
  }
  column "network_weight_fraction" {
    null = true
    type = double_precision
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "network_status_entry" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.weights_id]
  }
  foreign_key "network_status_entry_weights_network_status_entry_fkey" {
    columns     = [column.network_status_entry]
    ref_columns = [table.network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "network_status_entry_weights_network_status_entry_idx" {
    columns = [column.network_status_entry]
  }
  index "network_status_entry_weights_published_idx" {
    columns = [column.published]
  }
}
table "network_status_totals" {
  schema = schema.public
  column "totals_id" {
    null    = false
    type    = uuid
    default = sql("uuid_generate_v4()")
  }
  column "total_consensus_weight" {
    null = true
    type = double_precision
  }
  column "total_guard_weight" {
    null = true
    type = double_precision
  }
  column "total_middle_weight" {
    null = true
    type = double_precision
  }
  column "total_exit_weight" {
    null = true
    type = double_precision
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "network_status" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.totals_id]
  }
  foreign_key "network_status_totals_network_status_fkey" {
    columns     = [column.network_status]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "network_status_totals_network_status_idx" {
    columns = [column.network_status]
  }
  index "network_status_totals_published_idx" {
    columns = [column.published]
  }
}
table "network_vote" {
  schema = schema.public
  column "header" {
    null = false
    type = text
  }
  column "network_status_version" {
    null = false
    type = integer
  }
  column "consensus_methods" {
    null = true
    type = text
  }
  column "published" {
    null = true
    type = timestamp
  }
  column "valid_after" {
    null = true
    type = timestamp
  }
  column "fresh_until" {
    null = true
    type = timestamp
  }
  column "valid_until" {
    null = true
    type = timestamp
  }
  column "vote_seconds" {
    null = true
    type = bigint
  }
  column "dist_seconds" {
    null = true
    type = bigint
  }
  column "known_flags" {
    null = false
    type = text
  }
  column "recommended_client_versions" {
    null = true
    type = text
  }
  column "recommended_server_versions" {
    null = true
    type = text
  }
  column "recommended_client_protocols" {
    null = false
    type = text
  }
  column "recommended_relay_protocols" {
    null = false
    type = text
  }
  column "required_client_protocols" {
    null = false
    type = text
  }
  column "required_relay_protocols" {
    null = false
    type = text
  }
  column "params" {
    null = false
    type = text
  }
  column "package_lines" {
    null = false
    type = text
  }
  column "shared_rand_previous_value" {
    null = true
    type = text
  }
  column "shared_rand_current_value" {
    null = true
    type = text
  }
  column "shared_rand_previous_num" {
    null = true
    type = text
  }
  column "shared_rand_current_num" {
    null = true
    type = text
  }
  column "stable_uptime" {
    null = true
    type = bigint
  }
  column "stable_mtbf" {
    null = true
    type = bigint
  }
  column "fast_bandwidth" {
    null = true
    type = bigint
  }
  column "guard_wfu" {
    null = true
    type = double_precision
  }
  column "guard_tk" {
    null = true
    type = bigint
  }
  column "guard_bandwidth_including_exits" {
    null = true
    type = bigint
  }
  column "guard_bandwidth_excluding_exits" {
    null = true
    type = bigint
  }
  column "enough_mtbf_info" {
    null = true
    type = integer
  }
  column "ignoring_adv_bws" {
    null = true
    type = integer
  }
  column "nickname" {
    null = true
    type = text
  }
  column "identity" {
    null = true
    type = text
  }
  column "hostname" {
    null = true
    type = text
  }
  column "address" {
    null = true
    type = text
  }
  column "dir_port" {
    null = true
    type = integer
  }
  column "or_port" {
    null = true
    type = integer
  }
  column "contact" {
    null = true
    type = text
  }
  column "shared_rand_participate" {
    null = true
    type = boolean
  }
  column "shared_rand_commit_lines" {
    null = true
    type = text
  }
  column "bandwidth_file_headers" {
    null = true
    type = text
  }
  column "bandwidth_file_digest" {
    null = true
    type = text
  }
  column "dir_key_cert_version" {
    null = true
    type = integer
  }
  column "legacy_dir_key" {
    null = true
    type = text
  }
  column "dir_identity_key" {
    null = true
    type = text
  }
  column "dir_key_published" {
    null = true
    type = timestamp
  }
  column "dir_key_expires" {
    null = true
    type = timestamp
  }
  column "dir_signing_key" {
    null = true
    type = text
  }
  column "dir_key_cross_cert" {
    null = true
    type = text
  }
  column "dir_key_certification" {
    null = true
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "directory_signatures" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "network_vote_published" {
    columns = [column.published]
  }
}
table "network_vote_document" {
  schema = schema.public
  column "network_vote_digest" {
    null = true
    type = text
  }
  column "network_vote_entry_digest" {
    null = true
    type = text
  }
  foreign_key "network_vote_document_network_vote_digest_fkey" {
    columns     = [column.network_vote_digest]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "network_vote_document_network_vote_entry_digest_fkey" {
    columns     = [column.network_vote_entry_digest]
    ref_columns = [table.network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "network_vote_entry" {
  schema = schema.public
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "ip" {
    null = false
    type = text
  }
  column "or_port" {
    null = false
    type = integer
  }
  column "dir_port" {
    null = false
    type = integer
  }
  column "or_addresses" {
    null = false
    type = text
  }
  column "flags" {
    null = true
    type = text
  }
  column "version" {
    null = true
    type = text
  }
  column "bandwidth_measured" {
    null = true
    type = bigint
  }
  column "bandwidth_unmeasured" {
    null = true
    type = boolean
  }
  column "bandwidth" {
    null = true
    type = bigint
  }
  column "proto" {
    null = true
    type = text
  }
  column "policy" {
    null = true
    type = text
  }
  column "port_list" {
    null = true
    type = text
  }
  column "master_key_ed25519" {
    null = true
    type = text
  }
  column "supported_consensus_methods" {
    null = true
    type = text
  }
  column "network_vote" {
    null = true
    type = text
  }
  column "server_descriptor_digest" {
    null = true
    type = text
  }
  column "m_lines" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  foreign_key "network_vote_entry_network_vote_fkey" {
    columns     = [column.network_vote]
    ref_columns = [table.network_vote.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "network_vote_entry_fingerprint" {
    columns = [column.fingerprint]
  }
  index "network_vote_entry_nickname" {
    columns = [column.nickname]
  }
  index "network_vote_entry_published" {
    columns = [column.published]
  }
}
table "onionperf_analysis" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "measurement_ip" {
    null = false
    type = text
  }
  column "type" {
    null = false
    type = text
  }
  column "version" {
    null = false
    type = text
  }
  column "filters" {
    null = false
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
  index "onionperf_analysis_published_idx" {
    columns = [column.published]
  }
}
table "onionperf_measurement" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "stream_id" {
    null = false
    type = text
  }
  column "tor_stream_id" {
    null = false
    type = text
  }
  column "circuit_id" {
    null = false
    type = text
  }
  column "filesize_bytes" {
    null = true
    type = integer
  }
  column "payload_progress_100" {
    null = true
    type = double_precision
  }
  column "payload_progress_80" {
    null = true
    type = double_precision
  }
  column "time_to_first_byte" {
    null = true
    type = bigint
  }
  column "time_to_last_byte" {
    null = true
    type = bigint
  }
  column "error_code" {
    null = true
    type = text
  }
  column "endpoint_local" {
    null = true
    type = text
  }
  column "unix_ts_end" {
    null = true
    type = timestamp
  }
  column "unix_ts_start" {
    null = false
    type = timestamp
  }
  column "mbps" {
    null = true
    type = double_precision
  }
  column "path" {
    null = true
    type = text
  }
  column "cbt_set" {
    null = true
    type = boolean
  }
  column "filtered_out" {
    null = true
    type = boolean
  }
  column "is_onion" {
    null = true
    type = boolean
  }
  column "middle" {
    null = true
    type = text
  }
  column "guard" {
    null = true
    type = text
  }
  column "exit" {
    null = true
    type = text
  }
  column "onionperf_analysis" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.onionperf_node, column.circuit_id, column.stream_id, column.unix_ts_start]
  }
  foreign_key "onionperf_measurement_onionperf_analysis_fkey" {
    columns     = [column.onionperf_analysis]
    ref_columns = [table.onionperf_analysis.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "onionperf_tgen_stream" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "stream_id" {
    null = false
    type = text
  }
  column "byte_info_payload_bytes_recv" {
    null = true
    type = bigint
  }
  column "byte_info_payload_bytes_send" {
    null = true
    type = bigint
  }
  column "byte_info_payload_progress_recv" {
    null = true
    type = text
  }
  column "byte_info_payload_progress_send" {
    null = true
    type = text
  }
  column "byte_info_total_bytes_recv" {
    null = true
    type = bigint
  }
  column "byte_info_total_bytes_send" {
    null = true
    type = bigint
  }
  column "elapsed_seconds_payload_bytes_recv" {
    null = true
    type = text
  }
  column "elapsed_seconds_payload_bytes_send" {
    null = true
    type = text
  }
  column "elapsed_seconds_payload_progress_recv" {
    null = true
    type = text
  }
  column "elapsed_seconds_payload_progress_sent" {
    null = true
    type = text
  }
  column "is_complete" {
    null = true
    type = boolean
  }
  column "is_error" {
    null = true
    type = boolean
  }
  column "is_success" {
    null = true
    type = boolean
  }
  column "stream_info_error" {
    null = true
    type = text
  }
  column "stream_info_id" {
    null = true
    type = bigint
  }
  column "stream_info_name" {
    null = true
    type = text
  }
  column "stream_info_peername" {
    null = true
    type = text
  }
  column "stream_info_recvsize" {
    null = true
    type = bigint
  }
  column "stream_info_recvstate" {
    null = true
    type = text
  }
  column "stream_info_sendsize" {
    null = true
    type = bigint
  }
  column "stream_info_sendstate" {
    null = true
    type = text
  }
  column "stream_info_vertexid" {
    null = true
    type = text
  }
  column "time_info_created_ts" {
    null = true
    type = bigint
  }
  column "time_info_now_ts" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_checksum_recv" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_checksum_send" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_command" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_first_byte_recv" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_first_byte_send" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_last_byte_recv" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_last_byte_send" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_proxy_choice" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_proxy_init" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_proxy_request" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_proxy_response" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_response" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_socket_connect" {
    null = true
    type = bigint
  }
  column "time_info_usecs_to_socket_create" {
    null = true
    type = bigint
  }
  column "transport_info_error" {
    null = true
    type = text
  }
  column "transport_info_fd" {
    null = true
    type = bigint
  }
  column "transport_info_local" {
    null = true
    type = text
  }
  column "transport_info_proxy" {
    null = true
    type = text
  }
  column "transport_info_remote" {
    null = true
    type = text
  }
  column "transport_info_state" {
    null = true
    type = text
  }
  column "unix_ts_end" {
    null = true
    type = timestamp
  }
  column "unix_ts_start" {
    null = false
    type = timestamp
  }
  column "onionperf_analysis" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.onionperf_node, column.stream_id, column.unix_ts_start]
  }
  foreign_key "onionperf_tgen_stream_onionperf_analysis_fkey" {
    columns     = [column.onionperf_analysis]
    ref_columns = [table.onionperf_analysis.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "onionperf_tor_circuit" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "circuit_id" {
    null = false
    type = text
  }
  column "build_quantile" {
    null = true
    type = double_precision
  }
  column "build_timeout" {
    null = true
    type = bigint
  }
  column "buildtime_seconds" {
    null = true
    type = double_precision
  }
  column "cbt_set" {
    null = true
    type = boolean
  }
  column "current_guards" {
    null = true
    type = text
  }
  column "elapsed_seconds" {
    null = true
    type = text
  }
  column "failure_reason_local" {
    null = true
    type = text
  }
  column "filtered_out" {
    null = true
    type = boolean
  }
  column "circuit_path" {
    null = true
    type = text
  }
  column "unix_ts_end" {
    null = true
    type = timestamp
  }
  column "unix_ts_start" {
    null = false
    type = timestamp
  }
  column "onionperf_analysis" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.onionperf_node, column.circuit_id, column.unix_ts_start]
  }
  foreign_key "onionperf_tor_circuit_onionperf_analysis_fkey" {
    columns     = [column.onionperf_analysis]
    ref_columns = [table.onionperf_analysis.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "onionperf_tor_circuit_onionperf_analysis_idx" {
    columns = [column.onionperf_analysis]
  }
}
table "onionperf_tor_guard" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "country" {
    null = true
    type = text
  }
  column "dropped_ts" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "nickname" {
    null = false
    type = text
  }
  column "onionperf_analysis" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.onionperf_node, column.fingerprint, column.nickname, column.dropped_ts]
  }
  foreign_key "onionperf_tor_guard_onionperf_analysis_fkey" {
    columns     = [column.onionperf_analysis]
    ref_columns = [table.onionperf_analysis.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "onionperf_tor_stream" {
  schema = schema.public
  column "onionperf_node" {
    null = false
    type = text
  }
  column "stream_id" {
    null = false
    type = text
  }
  column "circuit_id" {
    null = false
    type = text
  }
  column "elapsed_seconds" {
    null = true
    type = text
  }
  column "source" {
    null = true
    type = text
  }
  column "target" {
    null = true
    type = text
  }
  column "unix_ts_end" {
    null = true
    type = timestamp
  }
  column "unix_ts_start" {
    null = false
    type = timestamp
  }
  column "onionperf_analysis" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.onionperf_node, column.stream_id, column.circuit_id, column.unix_ts_start]
  }
  foreign_key "onionperf_tor_stream_onionperf_analysis_fkey" {
    columns     = [column.onionperf_analysis]
    ref_columns = [table.onionperf_analysis.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
}
table "scanner_countries" {
  schema = schema.public
  column "scanner_country" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.scanner_country]
  }
}
table "server_descriptor" {
  schema = schema.public
  column "is_bridge" {
    null = false
    type = boolean
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "digest_sha1_hex" {
    null = false
    type = text
  }
  column "identity_ed25519" {
    null = true
    type = text
  }
  column "master_key_ed25519" {
    null = true
    type = text
  }
  column "address" {
    null = true
    type = text
  }
  column "or_port" {
    null = true
    type = text
  }
  column "socks_port" {
    null = true
    type = text
  }
  column "dir_port" {
    null = true
    type = text
  }
  column "or_addresses" {
    null = true
    type = text
  }
  column "bandwidth_avg" {
    null = true
    type = bigint
  }
  column "bandwidth_burst" {
    null = true
    type = bigint
  }
  column "bandwidth_observed" {
    null = true
    type = bigint
  }
  column "platform" {
    null = true
    type = text
  }
  column "overload_general_timestamp" {
    null = true
    type = timestamp
  }
  column "overload_general_version" {
    null = true
    type = integer
  }
  column "protocols" {
    null = true
    type = text
  }
  column "is_hibernating" {
    null = true
    type = boolean
  }
  column "uptime" {
    null = true
    type = bigint
  }
  column "onion_key" {
    null = true
    type = text
  }
  column "signing_key" {
    null = true
    type = text
  }
  column "exit_policy" {
    null = true
    type = text
  }
  column "contact" {
    null = true
    type = text
  }
  column "bridge_distribution_request" {
    null = true
    type = text
  }
  column "family" {
    null = true
    type = text
  }
  column "read_history" {
    null = true
    type = text
  }
  column "write_history" {
    null = true
    type = text
  }
  column "uses_enhanced_dns_logic" {
    null = true
    type = boolean
  }
  column "caches_extra_info" {
    null = true
    type = boolean
  }
  column "extra_info_sha1_digest" {
    null = true
    type = text
  }
  column "extra_info_sha256_digest" {
    null = true
    type = text
  }
  column "is_hidden_service_dir" {
    null = true
    type = boolean
  }
  column "link_protocol_version" {
    null = true
    type = sql("integer[]")
  }
  column "circuit_protocol_version" {
    null = true
    type = sql("integer[]")
  }
  column "allow_single_hop_exits" {
    null = true
    type = boolean
  }
  column "ipv6_default_policy" {
    null = true
    type = text
  }
  column "ipv6_port_list" {
    null = true
    type = text
  }
  column "ntor_onion_key" {
    null = true
    type = text
  }
  column "onion_key_cross_cert" {
    null = true
    type = text
  }
  column "ntor_onion_key_cross_cert" {
    null = true
    type = text
  }
  column "ntor_onion_key_cross_cert_sign" {
    null = true
    type = integer
  }
  column "tunnelled_dir_server" {
    null = true
    type = boolean
  }
  column "router_sig_ed25519" {
    null = true
    type = text
  }
  column "router_signature" {
    null = true
    type = text
  }
  column "header" {
    null = true
    type = text
  }
  column "digest_sha256" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.digest_sha1_hex]
  }
  index "server_descriptor_published" {
    columns = [column.published]
  }
  index "server_fingerprint_published" {
    on {
      column = column.fingerprint
    }
    on {
      desc   = true
      column = column.published
    }
  }
}
table "server_families" {
  schema = schema.public
  column "digest" {
    null = false
    type = text
  }
  column "effective_family" {
    null = true
    type = text
  }
  column "members" {
    null = true
    type = integer
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "updated" {
    null = false
    type = timestamp
  }
  primary_key {
    columns = [column.digest]
  }
  index "server_families_effective_family_trgm_idx" {
    type = GIN
    on {
      column = column.effective_family
      ops    = gin_trgm_ops
    }
  }
}
table "server_log" {
  schema = schema.public
  column "published" {
    null = false
    type = timestamp
  }
  column "physical_host" {
    null = true
    type = text
  }
  column "virtual_host" {
    null = true
    type = text
  }
  column "digest" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.digest]
  }
}
table "server_note" {
  schema = schema.public
  column "note_id" {
    null = false
    type = bigint
    identity {
      generated = ALWAYS
    }
  }
  column "note" {
    null = false
    type = text
  }
  column "username" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "edited" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "status" {
    null = false
    type = text
  }
  primary_key {
    columns = [column.note_id, column.fingerprint]
  }
}
table "server_status" {
  schema = schema.public
  column "is_bridge" {
    null = false
    type = boolean
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "nickname" {
    null = false
    type = text
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "or_addresses" {
    null = true
    type = text
  }
  column "last_seen" {
    null = false
    type = timestamp
  }
  column "first_seen" {
    null = false
    type = timestamp
  }
  column "running" {
    null = true
    type = boolean
  }
  column "flags" {
    null = true
    type = text
  }
  column "country" {
    null = true
    type = text
  }
  column "country_name" {
    null = true
    type = text
  }
  column "autonomous_system" {
    null = true
    type = text
  }
  column "as_name" {
    null = true
    type = text
  }
  column "verified_host_names" {
    null = true
    type = text
  }
  column "last_restarted" {
    null = false
    type = timestamp
  }
  column "exit_policy" {
    null = true
    type = text
  }
  column "contact" {
    null = true
    type = text
  }
  column "platform" {
    null = true
    type = text
  }
  column "version" {
    null = true
    type = text
  }
  column "version_status" {
    null = true
    type = text
  }
  column "effective_family" {
    null = true
    type = text
  }
  column "declared_family" {
    null = true
    type = text
  }
  column "transport" {
    null = true
    type = text
  }
  column "bridgedb_distributor" {
    null = true
    type = text
  }
  column "blocklist" {
    null = true
    type = text
  }
  column "last_changed_address_or_port" {
    null = true
    type = timestamp
  }
  column "diff_or_addresses" {
    null = true
    type = text
  }
  column "unverified_host_names" {
    null = true
    type = text
  }
  column "unreachable_or_addresses" {
    null = true
    type = text
  }
  column "overload_ratelimits_version" {
    null = true
    type = integer
  }
  column "overload_ratelimits_timestamp" {
    null = true
    type = timestamp
  }
  column "overload_ratelimits_ratelimit" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_burstlimit" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_read_count" {
    null = true
    type = bigint
  }
  column "overload_ratelimits_write_count" {
    null = true
    type = bigint
  }
  column "overload_fd_exhausted_version" {
    null = true
    type = integer
  }
  column "overload_fd_exhausted_timestamp" {
    null = true
    type = timestamp
  }
  column "overload_general_timestamp" {
    null = true
    type = timestamp
  }
  column "overload_general_version" {
    null = true
    type = integer
  }
  column "first_network_status_digest" {
    null = true
    type = text
  }
  column "first_network_status_entry_digest" {
    null = true
    type = text
  }
  column "last_network_status_digest" {
    null = true
    type = text
  }
  column "last_network_status_entry_digest" {
    null = true
    type = text
  }
  column "first_bridge_network_status_digest" {
    null = true
    type = text
  }
  column "first_bridge_network_status_entry_digest" {
    null = true
    type = text
  }
  column "last_bridge_network_status_digest" {
    null = true
    type = text
  }
  column "last_bridge_network_status_entry_digest" {
    null = true
    type = text
  }
  column "last_network_status_weights_id" {
    null = true
    type = uuid
  }
  column "last_network_status_totals_id" {
    null = true
    type = uuid
  }
  column "last_server_descriptor_digest" {
    null = true
    type = text
  }
  column "last_extrainfo_descriptor_digest" {
    null = true
    type = text
  }
  column "last_bridgestrap_stats_digest" {
    null = true
    type = text
  }
  column "last_bridgestrap_test_digest" {
    null = true
    type = text
  }
  column "last_bridge_pool_assignments_file_digest" {
    null = true
    type = text
  }
  column "last_bridge_pool_assignment_digest" {
    null = true
    type = text
  }
  column "family_digest" {
    null = true
    type = text
  }
  column "advertised_bandwidth" {
    null = true
    type = bigint
  }
  column "bandwidth" {
    null = true
    type = bigint
  }
  column "network_weight_fraction" {
    null = true
    type = double_precision
  }
  column "bandwidth_avg" {
    null = true
    type = bigint
  }
  column "bandwidth_burst" {
    null = true
    type = bigint
  }
  column "bandwidth_observed" {
    null = true
    type = bigint
  }
  column "ipv6_default_policy" {
    null = true
    type = text
  }
  column "ipv6_port_list" {
    null = true
    type = text
  }
  column "socks_port" {
    null = true
    type = integer
  }
  column "dir_port" {
    null = true
    type = integer
  }
  column "or_port" {
    null = true
    type = integer
  }
  column "is_hibernating" {
    null = true
    type = boolean
  }
  column "address" {
    null = true
    type = text
  }
  column "is_stale" {
    null = true
    type = boolean
  }
  primary_key {
    columns = [column.fingerprint, column.nickname, column.published]
  }
  foreign_key "server_status_family_digest_fkey" {
    columns     = [column.family_digest]
    ref_columns = [table.server_families.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_first_bridge_network_status_digest_fkey" {
    columns     = [column.first_bridge_network_status_digest]
    ref_columns = [table.bridge_network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_first_bridge_network_status_entry_digest_fkey" {
    columns     = [column.first_bridge_network_status_entry_digest]
    ref_columns = [table.bridge_network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_first_network_status_digest_fkey" {
    columns     = [column.first_network_status_digest]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_first_network_status_entry_digest_fkey" {
    columns     = [column.first_network_status_entry_digest]
    ref_columns = [table.network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridge_network_status_digest_fkey" {
    columns     = [column.last_bridge_network_status_digest]
    ref_columns = [table.bridge_network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridge_network_status_entry_digest_fkey" {
    columns     = [column.last_bridge_network_status_entry_digest]
    ref_columns = [table.bridge_network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridge_pool_assignment_digest_fkey" {
    columns     = [column.last_bridge_pool_assignment_digest]
    ref_columns = [table.bridge_pool_assignment.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridge_pool_assignments_file_digest_fkey" {
    columns     = [column.last_bridge_pool_assignments_file_digest]
    ref_columns = [table.bridge_pool_assignments_file.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridgestrap_stats_digest_fkey" {
    columns     = [column.last_bridgestrap_stats_digest]
    ref_columns = [table.bridgestrap_stats.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_bridgestrap_test_digest_fkey" {
    columns     = [column.last_bridgestrap_test_digest]
    ref_columns = [table.bridgestrap_test.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_extrainfo_descriptor_digest_fkey" {
    columns     = [column.last_extrainfo_descriptor_digest]
    ref_columns = [table.extra_info_descriptor.column.digest_sha1_hex]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_network_status_digest_fkey" {
    columns     = [column.last_network_status_digest]
    ref_columns = [table.network_status.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_network_status_entry_digest_fkey" {
    columns     = [column.last_network_status_entry_digest]
    ref_columns = [table.network_status_entry.column.digest]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_network_status_totals_id_fkey" {
    columns     = [column.last_network_status_totals_id]
    ref_columns = [table.network_status_totals.column.totals_id]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_network_status_weights_id_fkey" {
    columns     = [column.last_network_status_weights_id]
    ref_columns = [table.network_status_entry_weights.column.weights_id]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  foreign_key "server_status_last_server_descriptor_digest_fkey" {
    columns     = [column.last_server_descriptor_digest]
    ref_columns = [table.server_descriptor.column.digest_sha1_hex]
    on_update   = NO_ACTION
    on_delete   = NO_ACTION
  }
  index "server_status_family_fingerprint_nickname_published_idx" {
    on {
      column = column.family_digest
    }
    on {
      column = column.fingerprint
    }
    on {
      column = column.nickname
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "server_status_fingerprint" {
    columns = [column.fingerprint]
  }
  index "server_status_fingerprint_nickname_published_dirauth_idx" {
    where = "((is_bridge = false) AND (flags ~ 'Authority'::text))"
    on {
      column = column.fingerprint
    }
    on {
      column = column.nickname
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "server_status_flags_idx" {
    columns = [column.flags]
  }
  index "server_status_is_bridge" {
    columns = [column.is_bridge]
  }
  index "server_status_nickname" {
    columns = [column.nickname]
  }
  index "server_status_optimized_index" {
    on {
      column = column.fingerprint
    }
    on {
      column = column.nickname
    }
    on {
      desc   = true
      column = column.published
    }
  }
  index "server_status_published" {
    columns = [column.published]
  }
  index "server_status_published_flags_idx" {
    columns = [column.published, column.flags]
  }
  index "server_status_running" {
    columns = [column.running]
  }
}
table "server_tag" {
  schema = schema.public
  column "tag" {
    null = false
    type = text
  }
  column "username" {
    null = false
    type = text
  }
  column "published" {
    null = false
    type = timestamp
  }
  column "edited" {
    null = false
    type = timestamp
  }
  column "fingerprint" {
    null = false
    type = text
  }
  column "status" {
    null = false
    type = text
  }
  column "digest" {
    null = true
    type = text
  }
  primary_key {
    columns = [column.tag, column.fingerprint]
  }
}
schema "public" {
  comment = "standard public schema"
}
