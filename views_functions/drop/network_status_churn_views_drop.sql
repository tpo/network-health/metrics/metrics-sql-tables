-- Relays' Consensus Churn Rate DROP views, functions
-- ==================================================

DROP MATERIALIZED VIEW IF EXISTS hourly_relays_cw_flags CASCADE;

DROP FUNCTION IF EXISTS hourly_lost_relays_by_flag CASCADE;
DROP FUNCTION IF EXISTS hourly_number_relays_by_flag CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_guard CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_exit CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_hsdir CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_stable CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_valid CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_v2dir CASCADE;

DROP FUNCTION IF EXISTS hourly_new_relays_by_flag CASCADE;
DROP FUNCTION IF EXISTS hourly_start_end_number_relays_by_flag CASCADE;

DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_guard CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_exit CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_hsdir CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_stable CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_valid CASCADE;
DROP MATERIALIZED VIEW IF EXISTS relays_churn_rate_new_v2dir CASCADE;

-- Indexes
DROP INDEX IF EXISTS hourly_relays_cw_flags_valid_after_hour_fingerprint_idx;
DROP INDEX IF EXISTS relays_churn_rate_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_guard_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_exit_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_hsdir_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_stable_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_valid_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_v2dir_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_guard_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_exit_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_hsdir_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_stable_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_valid_end_hour_churn_lost_idx;
DROP INDEX IF EXISTS relays_churn_rate_v2dir_end_hour_churn_lost_idx;
