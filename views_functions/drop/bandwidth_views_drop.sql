-- Inflation detection bandwidth DROP views, functions
-- ===================================================

-- Functions
DROP FUNCTION IF EXISTS lowest_ratio_highest_weight CASCADE;
DROP FUNCTION IF EXISTS highest_weight_lowest_ratio CASCADE;
DROP FUNCTION IF EXISTS weight_gt_stream CASCADE;
DROP FUNCTION IF EXISTS bw_avgs CASCADE;

DROP FUNCTION IF EXISTS get_last_nick;
