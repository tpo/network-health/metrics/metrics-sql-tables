

-- From online_relays_views.sql

DROP VIEW IF EXISTS all_fingerprints CASCADE;
DROP VIEW IF EXISTS relays_online CASCADE;

DROP INDEX IF EXISTS all_fingerprints_fingerprint_idx;
DROP INDEX IF EXISTS relays_online_fingerprint_timestamp_idx;
