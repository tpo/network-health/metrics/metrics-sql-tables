-- Inflation detection several tables CREATE views, functions
-- ==========================================================

-- SELECT * FROM stream_gt_throughput('2024-03-29', '2024-03-31', 10, ARRAY['DE', 'US', 'SE', 'AT'], ARRAY['op-de8a']);
--                fingerprint                | scanner_country | onionperf_node | stream_bw_avg | throughput_avg | stream_by_throughput 
-- ------------------------------------------+-----------------+----------------+---------------+----------------+----------------------
--  4AA758D731D641923FAD1CF8F125A6412948DC13 | SE              | op-de8a        |       4543512 |          56795 |    79.99876996472166
--  4AA758D731D641923FAD1CF8F125A6412948DC13 | DE              | op-de8a        |       3283826 |          56795 |    57.81915856680297
--  FA85D84F61DCC9E5FAAC177808226A2EDC32B89B | SE              | op-de8a        |       5246810 |         125374 |     41.8491375785583
--  4AA758D731D641923FAD1CF8F125A6412948DC13 | US              | op-de8a        |       2345553 |          56795 |   41.298747507888784
--  FA1845C313B88EF49E8BE1F23B02CD0AC702B636 | SE              | op-de8a        |       1428233 |          39844 |    35.84558418928526
--  FA85D84F61DCC9E5FAAC177808226A2EDC32B89B | DE              | op-de8a        |       3884129 |         125374 |   30.980243022687702
--  5D98A8A2F60F26C65E34F4205BE77219E10EFB09 | DE              | op-de8a        |       1688628 |          56795 |   29.732102155334466
--  0F4130EDAEE634D96577E5A029637399BC4B1EA7 | SE              | op-de8a        |       1477319 |          63476 |   23.273680279937746
--  03E245B5CCA337284891026FBD90DAD1B37F6BBB | SE              | op-de8a        |       2159063 |          93529 |   23.084541830740935
--  84F3A0D8B8E7176A01FFDAE08350FC241A119D18 | DE              | op-de8a        |       5665876 |         268721 |   21.084619784127906
CREATE OR REPLACE FUNCTION stream_gt_throughput (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    lim INT,
    scanner_country TEXT[],
    onionperf_node TEXT[]
)
RETURNS TABLE
(
    fingerprint TEXT,
    scanner_country TEXT,
    onionperf_node TEXT,
    stream_bw_avg BIGINT,
    throughput_avg BIGINT,
    stream_by_throughput DOUBLE PRECISION
) AS
$body$
    SELECT DISTINCT
        bw_avgs.fingerprint,
        bw_avgs.scanner_country,
        throughput_avgs.onionperf_node,
        avg(bw_avgs.stream_bw_avg) AS stream_bw_avg_avg,
        avg(throughput_avgs.throughput_avg) AS throughput_avg_avg,
        avg(bw_avgs.stream_bw_avg) / avg(throughput_avgs.throughput_avg) AS stream_by_throughput
    FROM bw_avgs(datestart, dateend) AS bw_avgs
    INNER JOIN throughput_avgs(datestart, dateend) AS throughput_avgs
        ON bw_avgs.fingerprint = throughput_avgs.fingerprint
    WHERE scanner_country = ANY ($4)
    AND onionperf_node = ANY ($5)
    GROUP BY bw_avgs.fingerprint, bw_avgs.scanner_country, throughput_avgs.onionperf_node
    ORDER BY stream_by_throughput DESC
    LIMIT lim
$body$
LANGUAGE sql;

-- SELECT * FROM relay_other_data('2024-03-01', '2024-04-01', '021296AB765B9808EA81C3FAD3E19B0CC080603A')
--  nickname | overload_general_timestamp |  relay_type  |     first_seen      | country |       as_name       |                                                                                                                                                                                   declared_family
-- ----------+----------------------------+--------------+---------------------+---------+---------------------+-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--  0x90     |                            | stable_guard | 2024-03-30 20:00:00 | de      | Hetzner Online GmbH | "[\"$021296AB765B9808EA81C3FAD3E19B0CC080603A\",\"$233830F0A052F3845789607D1EC15457CF15F9AE\",\"$3ED6E3EAE2B41D2BCB0DC5652C34AF782A1063A9\",\"$42A955B09A4E327FBFB46A08F6E21705271CCA12\",\"$57490DBC1351DEC37E99756319177DD69373DFC4\",\"$70CDF3509A2063AF5C5C548F77793DACB6986AF9\",\"$948C3E8F4EF7D0E8EE19FADCED4ED287620B6FFA\",\"$C7790150FA3B4E0A9B2C5C021B8F0722AD993DF7\"]"
CREATE OR REPLACE FUNCTION relay_other_data (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    fp TEXT
)
RETURNS TABLE
(
    nickname TEXT,
    overload_general_timestamp TIMESTAMP WITHOUT TIME ZONE,
    relay_type TEXT,
    first_seen TIMESTAMP WITHOUT TIME ZONE,
    country TEXT,
    as_name TEXT,
    declared_family TEXT
) AS
$body$
    SELECT DISTINCT
        nickname,
        CASE WHEN overload_general_version > 0 THEN overload_general_timestamp END AS overloaded,
        relay_type,
        first_seen,
        country,
        as_name,
        declared_family
    FROM server_status
    INNER JOIN relay_type
    using (fingerprint)
    WHERE relay_type.published BETWEEN datestart AND dateend
    AND server_status.published BETWEEN datestart AND dateend
    AND fingerprint=fp
$body$
LANGUAGE sql;

CREATE OR REPLACE FUNCTION get_sum_relays_flag(
    date_start TIMESTAMPTZ, 
    date_end TIMESTAMPTZ, 
    country_code TEXT,     -- Pass scanner_country dynamically
    limit_results INT      -- Pass the limit dynamically
)
RETURNS TABLE(
    fingerprint TEXT,
    scanner_country TEXT,
    ratio_avg NUMERIC,
    advertised_bandwidth_avg NUMERIC,
    desc_bw_obs_last_avg NUMERIC,
    stream_bw_avg NUMERIC,
    weight_bytes_avg NUMERIC,
    bandwidth_observed_avg NUMERIC
) AS $$
BEGIN
    RETURN QUERY 
    SELECT * FROM sum_relays_flag(
        date_start, 
        date_end,
        array(
            SELECT 
                fingerprint
            FROM (
                SELECT 
                    fingerprint, scanner_country, ratio_avg, advertised_bandwidth_avg
                FROM (
                    SELECT
                        scanner_country, 
                        fingerprint,
                        AVG(ratio) AS ratio_avg,
                        AVG(weight_bytes) AS weight_bytes_avg,                                  
                        AVG(advertised_bw) AS advertised_bw_avg,
                        AVG(desc_bw_obs_last) AS desc_bw_obs_last_avg,
                        AVG(stream_bw) AS stream_bw_avg,   
                        AVG(advertised_bandwidth) AS advertised_bandwidth_avg,
                        AVG(bandwidth_observed) AS bandwidth_observed_avg 
                    FROM bw_line 
                    WHERE published BETWEEN date_start AND date_end
                    AND running = true
                    AND stream_bw > -1                                                          
                    GROUP BY fingerprint, scanner_country
                ) as bw_avg
                WHERE scanner_country = country_code  -- Use country parameter for filtering
                AND ratio_avg > 0
            ) AS filtered_results
            ORDER BY advertised_bandwidth_avg DESC, ratio_avg
            LIMIT limit_results
        )
    );
END;
$$ LANGUAGE plpgsql;
