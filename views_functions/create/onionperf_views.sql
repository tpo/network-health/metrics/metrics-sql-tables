-- Inflation detection onionperf CREATE views, functions
-- =====================================================

-- Select all possible relays being measured in the middle position.
--
CREATE OR REPLACE VIEW fps_as_middle AS
SELECT DISTINCT middle AS fingerprint
FROM onionperf_measurement
WHERE onionperf_node NOT LIKE '%-conflux'
    AND middle NOT LIKE '';

-- Select all the relays that aren't being measured already as middle but are
-- being measured in the guard position.
--
CREATE OR REPLACE VIEW fps_as_guard AS
SELECT DISTINCT guard AS fingerprint
FROM onionperf_measurement
WHERE onionperf_node NOT LIKE '%-conflux'
    AND guard NOT LIKE ''
EXCEPT
SELECT fingerprint
FROM fps_as_middle;

-- Select all the relays that are being measured as middle or guard.
--
CREATE OR REPLACE VIEW fps_as_middle_or_guard AS
SELECT fingerprint
FROM fps_as_guard
UNION
SELECT fingerprint
FROM fps_as_middle;

-- Select all the relays that aren't being measured already as middle or guard
-- but are being measured in the exit position.
--
CREATE OR REPLACE VIEW fps_as_exit AS
SELECT DISTINCT exit AS fingerprint
FROM onionperf_measurement
WHERE onionperf_node NOT LIKE '%-conflux'
    AND exit NOT LIKE ''
EXCEPT
SELECT fingerprint
FROM fps_as_middle_or_guard;

-- View to store all the attributes for relays being measured as middle.
--
CREATE OR REPLACE VIEW as_middles AS
SELECT
    onionperf_measurement.middle AS fingerprint,
    onionperf_measurement.unix_ts_start,
    onionperf_measurement.onionperf_node,
    true AS as_middle,
    false AS as_guard,
    false AS as_exit,
    onionperf_measurement.mbps * 0.125 * 1000 * 1000 AS throughput
FROM onionperf_measurement
INNER JOIN fps_as_middle
    ON onionperf_measurement.middle = fps_as_middle.fingerprint
WHERE onionperf_measurement.onionperf_node NOT LIKE '%-conflux';

-- View to store all the attributes for relays being measured as guard.
--
CREATE OR REPLACE VIEW as_guards AS
SELECT
    onionperf_measurement.guard AS fingerprint,
    onionperf_measurement.unix_ts_start,
    onionperf_measurement.onionperf_node,
    false AS as_middle,
    true AS as_guard,
    false AS as_exit,
    onionperf_measurement.mbps * 0.125 * 1000 * 1000 AS throughput
FROM onionperf_measurement
INNER JOIN fps_as_guard
    ON onionperf_measurement.guard = fps_as_guard.fingerprint
WHERE onionperf_measurement.onionperf_node NOT LIKE '%-conflux';

-- View to store all the attributes for relays being measured as exit.
--
CREATE OR REPLACE VIEW as_exits AS
SELECT
    onionperf_measurement.exit AS fingerprint,
    onionperf_measurement.unix_ts_start,
    onionperf_measurement.onionperf_node,
    false AS as_middle,
    false AS as_guard,
    true AS as_exit,
    onionperf_measurement.mbps * 0.125 * 1000 * 1000 AS throughput
FROM onionperf_measurement
INNER JOIN fps_as_exit
    ON onionperf_measurement.exit = fps_as_exit.fingerprint
WHERE onionperf_measurement.onionperf_node NOT LIKE '%-conflux';

-- View to collect each relay throughput and whether they were in guard,
-- middle or exit position.
--
-- SELECT * FROM relay_throughput
-- WHERE onionperf_node='op-us8a'
-- AND fingerprint='000A10D43011EA4928A35F610405F92B4433B4DC';
--                fingerprint                |    unix_ts_start    | onionperf_node | as_middle | as_guard | as_exit |     throughput
-- ------------------------------------------+---------------------+----------------+-----------+----------+---------+--------------------
--  000A10D43011EA4928A35F610405F92B4433B4DC | 2024-03-22 00:55:31 | op-us8a        | t         | f        | f       |    303723.17053001
--  000A10D43011EA4928A35F610405F92B4433B4DC | 2024-03-22 00:55:01 | op-us8a        | t         | f        | f       | 385143.96030788706
CREATE OR REPLACE VIEW relay_throughput AS
SELECT DISTINCT
    fingerprint,
    unix_ts_start,
    onionperf_node,
    as_middle,
    as_guard,
    as_exit,
    throughput
FROM as_middles
UNION
SELECT
    fingerprint,
    unix_ts_start,
    onionperf_node,
    as_middle,
    as_guard,
    as_exit,
    throughput
FROM as_guards
UNION
SELECT
    fingerprint,
    unix_ts_start,
    onionperf_node,
    as_middle,
    as_guard,
    as_exit,
    throughput
FROM as_exits;

-- View to calculate network throughput averages by **week**, instead of by
-- hour like network stream averages, that are calculated by consensus and
-- therefore by **hour**. They're also calculated by onionperf node.
--
-- SELECT * FROM relay_throughput_avg
-- WHERE start_week='2024-03-18 00:00:00'
-- AND onionperf_node='op-us8a'
-- AND fingerprint='000A10D43011EA4928A35F610405F92B4433B4DC'
--      start_week      | onionperf_node |               fingerprint                |   throughput_avg
-- ---------------------+----------------+------------------------------------------+--------------------
--  2024-03-18 00:00:00 | op-us8a        | 000A10D43011EA4928A35F610405F92B4433B4DC | 344433.56541894854
CREATE OR REPLACE VIEW relay_throughput_avg AS
SELECT DISTINCT
    date_trunc('week', unix_ts_start) AS start_week,
    onionperf_node,
    fingerprint,
    avg(throughput) AS throughput_avg
FROM relay_throughput
GROUP BY fingerprint, onionperf_node, start_week;

-- Materialized view to obtain each relay throughput average filtered by
-- onionperf node.
--
-- SELECT * FROM relay_throughput_avg_filt
-- WHERE date_trunc('week', unix_ts_start)='2024-03-18 00:00:00'
-- AND onionperf_node='op-us8a'
-- AND fingerprint='000A10D43011EA4928A35F610405F92B4433B4DC';
--     unix_ts_start    | onionperf_node |               fingerprint                |     throughput     |  throughput_filt   | as_middle | as_guard | as_exit
-- ---------------------+----------------+------------------------------------------+--------------------+--------------------+-----------+----------+---------
--  2024-03-22 00:55:01 | op-us8a        | 000A10D43011EA4928A35F610405F92B4433B4DC | 385143.96030788706 | 385143.96030788706 | t         | f        | f
--  2024-03-22 00:55:31 | op-us8a        | 000A10D43011EA4928A35F610405F92B4433B4DC |    303723.17053001 | 344433.56541894854 | t         | f        | f
CREATE MATERIALIZED VIEW IF NOT EXISTS relay_throughput_avg_filt AS
SELECT DISTINCT
    relay_throughput.unix_ts_start,
    relay_throughput.onionperf_node,
    relay_throughput.fingerprint,
    relay_throughput.throughput,
    greatest(relay_throughput_avg.throughput_avg, relay_throughput.throughput)
    AS throughput_filt,
    as_middle,
    as_guard,
    as_exit
FROM relay_throughput
INNER JOIN relay_throughput_avg
    ON relay_throughput.fingerprint = relay_throughput_avg.fingerprint
        AND relay_throughput.onionperf_node = relay_throughput_avg.onionperf_node
        AND date_trunc('week', relay_throughput.unix_ts_start) = relay_throughput_avg.start_week
WITH DATA;

-- Materialized view to calculate network throughput averages and throughput
-- averages filtered by **week**. They're also calculated by onionperf node.
--
-- SELECT * FROM net_throughput
-- WHERE start_week='2024-03-18 00:00:00'
-- AND onionperf_node='op-us8a'
--      start_week      | onionperf_node | net_throughput_unfilt | net_throughput_filt |  net_throughput
-- ---------------------+----------------+-----------------------+---------------------+-------------------
--  2024-03-18 00:00:00 | op-us8a        |     614981.0014761596 |   731962.6786292269 | 731962.6786292269
CREATE MATERIALIZED VIEW IF NOT EXISTS net_throughput AS
SELECT
    date_trunc('week', unix_ts_start) as start_week,
    onionperf_node,
    avg(throughput) AS net_throughput_unfilt,
    avg(throughput_filt) AS net_throughput_filt,
    greatest(avg(throughput), avg(throughput_filt)) AS net_throughput
FROM relay_throughput_avg_filt
GROUP BY start_week, onionperf_node
ORDER BY start_week
WITH DATA;


-- Materialized view to obtain each relay ratio and throughput averages by
-- **week**. They're also calculated by onionperf node.
--
-- SELECT * FROM relay_throughput_ratio
-- WHERE unix_ts_start BETWEEN '2024-03-29' AND '2024-03-31'
-- AND fingerprint='26AD3C1C18F1CD2B357A33FA7652A906DB13A8CC'
CREATE MATERIALIZED VIEW IF NOT EXISTS relay_throughput_ratio AS
SELECT DISTINCT
    relay_throughput_avg_filt.unix_ts_start,
    relay_throughput_avg_filt.onionperf_node,
    relay_throughput_avg_filt.fingerprint,
    relay_throughput_avg_filt.throughput AS throughput_unfilt,
    relay_throughput_avg_filt.throughput_filt AS throughput_filt,
    greatest(relay_throughput_avg_filt.throughput, relay_throughput_avg_filt.throughput_filt) AS throughput,
    relay_throughput_avg_filt.throughput / net_throughput.net_throughput AS ratio_unfilt,
    relay_throughput_avg_filt.throughput_filt / net_throughput.net_throughput_filt AS ratio_filt,
    greatest(
        relay_throughput_avg_filt.throughput / net_throughput.net_throughput,
        relay_throughput_avg_filt.throughput_filt / net_throughput.net_throughput_filt
    ) AS ratio,
    as_middle,
    as_guard,
    as_exit
FROM net_throughput
INNER JOIN relay_throughput_avg_filt
    ON net_throughput.onionperf_node=relay_throughput_avg_filt.onionperf_node
    AND date_trunc('week', relay_throughput_avg_filt.unix_ts_start)=net_throughput.start_week
WITH DATA;

CREATE MATERIALIZED VIEW IF NOT EXISTS onionperf_nodes AS
SELECT DISTINCT onionperf_node
FROM onionperf_measurement
WHERE onionperf_node NOT LIKE '%-conflux'
WITH DATA;

-- Indexes
-- -------

CREATE UNIQUE INDEX IF NOT EXISTS relay_throughput_avg_filt_unix_ts_start_onionperf_node_fing_idx ON relay_throughput_avg_filt(unix_ts_start, onionperf_node, fingerprint);
CREATE UNIQUE INDEX IF NOT EXISTS relay_throughput_ratio_unix_ts_start_onionperf_node_fingerp_idx ON relay_throughput_ratio(unix_ts_start, onionperf_node, fingerprint);
CREATE INDEX IF NOT EXISTS relay_throughput_ratio_unix_ts_start_idx ON relay_throughput_ratio(unix_ts_start);
CREATE INDEX IF NOT EXISTS relay_throughput_ratio_onionperf_node_idx ON relay_throughput_ratio(onionperf_node);

-- Functions
-- ---------

-- Function to get each relay ratio and throughput average for a date/time
-- interval.
--
-- SELECT * FROM throughput_avgs('2024-03-29', '2024-03-31')
-- WHERE fingerprint = 'FC1E441E097BA36930AA2F615EFB325AF76D2595';
--  onionperf_node |               fingerprint                |  throughput_avg   |     ratio_avg      | as_middle | as_guard | as_exit
-- ----------------+------------------------------------------+-------------------+--------------------+-----------+----------+---------
--  op-hk8a        | FC1E441E097BA36930AA2F615EFB325AF76D2595 | 580766.6893934816 |  1.536881424662642 | t         | f        | f
--  op-us8a        | FC1E441E097BA36930AA2F615EFB325AF76D2595 | 692942.2985586341 | 1.0532986958263824 | t         | f        | f
CREATE OR REPLACE FUNCTION throughput_avgs(
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE
)
RETURNS TABLE
(
    onionperf_node TEXT,
    fingerprint TEXT,
    throughput_avg DOUBLE PRECISION,
    ratio_avg DOUBLE PRECISION,
    as_middle BOOL,
    as_guard BOOL,
    as_exit BOOL
) AS
$body$
SELECT DISTINCT
    onionperf_node,
    fingerprint,
    avg(throughput) AS throughput_avg,
    -- avg(throughput_unfilt) AS throughput_unfilt_avg,
    avg(ratio) AS ratio_avg,
    -- avg(ratio_unfilt) AS ratio_unfilt_avg,
    as_middle,
    as_guard,
    as_exit
FROM relay_throughput_ratio
WHERE unix_ts_start BETWEEN datestart AND dateend
GROUP BY onionperf_node, fingerprint, as_middle, as_guard, as_exit;
$body$
LANGUAGE sql;
