CREATE MATERIALIZED VIEW IF NOT EXISTS all_fingerprints AS
SELECT DISTINCT fingerprint
FROM server_descriptor
WITH DATA;

create unique index IF NOT EXISTS all_fingerprints_fingerprint_idx
on all_fingerprints(fingerprint);


CREATE MATERIALIZED VIEW IF NOT EXISTS relays_online AS
SELECT
    t.valid_after_hour AS timestamp,
    f.fingerprint,
    CASE
        WHEN hf.fingerprint IS NOT NULL THEN true  -- or 1 for integer
        ELSE false  -- or 0 for integer
    END AS status
FROM
    (SELECT DISTINCT valid_after_hour FROM hourly_relays_cw_flags) t
CROSS JOIN
    all_fingerprints f
LEFT JOIN
    hourly_relays_cw_flags hf
ON
    t.valid_after_hour = hf.valid_after_hour AND f.fingerprint = hf.fingerprint;

-- create unique index IF NOT EXISTS relays_online_fingerprint_idx
-- on all_fingerprints(fingerprint);
CREATE UNIQUE INDEX IF NOT EXISTS relays_online_fingerprint_timestamp_idx
ON relays_online(fingerprint, timestamp);
