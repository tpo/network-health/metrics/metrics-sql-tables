-- CDF ratio CREATE views
-- ======================

-- To speed up CDF ratio dashboard,

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_longclaw AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE dirauth_nickname = 'longclaw'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_longclaw_ratio_cdf_idx
  ON cdf_longclaw(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_longclaw_previous_month AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('month', current_date - INTERVAL '1 month')
  AND date_trunc('month', current_date)
AND dirauth_nickname = 'longclaw'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_longclaw_previous_month_ratio_cdf_idx
  ON cdf_longclaw_previous_month(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_longclaw_previous_2weeks AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '2 weeks')
  AND date_trunc('week', current_date)
AND dirauth_nickname = 'longclaw'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_longclaw_previous_2weeks_ratio_cdf_idx
  ON cdf_longclaw_previous_2weeks(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_longclaw_previous_week AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '1 week')
  AND date_trunc('week', current_date)
AND dirauth_nickname = 'longclaw'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_longclaw_previous_week_ratio_cdf_idx
  ON cdf_longclaw_previous_week(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_maatuska AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE scanner_country = 'SE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_maatuska_ratio_cdf_idx
  ON cdf_maatuska(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_maatuska_previous_month AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('month', current_date - INTERVAL '1 month')
  AND date_trunc('month', current_date)
AND scanner_country = 'SE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_maatuska_previous_month_ratio_cdf_idx
  ON cdf_maatuska_previous_month(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_maatuska_previous_2weeks AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '2 weeks')
  AND date_trunc('week', current_date)
AND scanner_country = 'SE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_maatuska_previous_2weeks_ratio_cdf_idx
  ON cdf_maatuska_previous_2weeks(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_maatuska_previous_week AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '1 week')
  AND date_trunc('week', current_date)
AND scanner_country = 'SE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_maatuska_previous_week_ratio_cdf_idx
  ON cdf_maatuska_previous_week(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_gabelmoo AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE scanner_country = 'DE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_gabelmoo_ratio_cdf_idx
  ON cdf_gabelmoo(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_gabelmoo_previous_month AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('month', current_date - INTERVAL '1 month')
  AND date_trunc('month', current_date)
AND scanner_country = 'DE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_gabelmoo_previous_month_ratio_cdf_idx
  ON cdf_gabelmoo_previous_month(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_gabelmoo_previous_2weeks AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '2 weeks')
  AND date_trunc('week', current_date)
AND scanner_country = 'DE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_gabelmoo_previous_2weeks_ratio_cdf_idx
  ON cdf_gabelmoo_previous_2weeks(ratio, cdf);


CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_gabelmoo_previous_week AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '1 week')
  AND date_trunc('week', current_date)
AND scanner_country = 'DE'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_gabelmoo_previous_week_ratio_cdf_idx
  ON cdf_gabelmoo_previous_week(ratio, cdf);


CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_tor26 AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE scanner_country = 'AT'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_tor26_ratio_cdf_idx
  ON cdf_tor26(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_tor26_previous_month AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('month', current_date - INTERVAL '1 month')
  AND date_trunc('month', current_date)
AND dirauth_nickname = 'AT'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_tor26_previous_month_ratio_cdf_idx
  ON cdf_tor26_previous_month(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_tor26_previous_2weeks AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '2 weeks')
  AND date_trunc('week', current_date)
AND dirauth_nickname = 'AT'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_tor26_previous_2weeks_ratio_cdf_idx
  ON cdf_tor26_previous_2weeks(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_tor26_previous_week AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '1 week')
  AND date_trunc('week', current_date)
AND dirauth_nickname = 'AT'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_tor26_previous_week_ratio_cdf_idx
  ON cdf_tor26_previous_week(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_bastet AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE scanner_country = 'US'
AND software_version = '1.8.1'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_bastet_ratio_cdf_idx
  ON cdf_bastet(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_bastet_previous_month AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('month', current_date - INTERVAL '1 month')
  AND date_trunc('month', current_date)
AND scanner_country = 'US'
AND software_version = '1.8.1'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_bastet_previous_month_ratio_cdf_idx
  ON cdf_bastet_previous_month(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_bastet_previous_2weeks AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '2 weeks')
  AND date_trunc('week', current_date)
AND scanner_country = 'US'
AND software_version = '1.8.1'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_bastet_previous_2weeks_ratio_cdf_idx
  ON cdf_bastet_previous_2weeks(ratio, cdf);

CREATE MATERIALIZED VIEW IF NOT EXISTS cdf_bastet_previous_week AS
SELECT DISTINCT ON (ratio)
    ratio,
    cume_dist() OVER (
        ORDER BY ratio
    ) AS cdf
FROM bw_line
WHERE time BETWEEN date_trunc('week', current_date - INTERVAL '1 week')
  AND date_trunc('week', current_date)
AND scanner_country = 'US'
AND software_version = '1.8.1'
ORDER BY ratio;

CREATE UNIQUE INDEX IF NOT EXISTS cdf_bastet_previous_week_ratio_cdf_idx
  ON cdf_bastet_previous_week(ratio, cdf);
