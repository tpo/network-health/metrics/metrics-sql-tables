-- Inflation detection bandwidth CREATE views, functions
-- =====================================================

-- Views AND functions to use FROM grafana without modifying the existing
-- tables that follow descriptor files scheme.
-- Views have to be `materialized` to be able to create indexes on the view
-- columns that don't exist in the original table

-- Functions
-- ---------

-- Function to obtain last nick FROM fp
-- eg:
-- SELECT * FROM get_last_nick ('F98CE40031795D3704365019EA9F8AC56AE2994B');
--     nick
-- ------------
--  torproxy02
CREATE OR REPLACE FUNCTION get_last_nick (fp TEXT)
RETURNS TABLE (nick TEXT) AS
$body$
  SELECT DISTINCT ON (nick) nick
  FROM bw_line
  WHERE fingerprint = $1
  ORDER BY nick, time DESC;
$body$
LANGUAGE sql;

-- SELECT * FROM bw_avgs('2024-03-29', '2024-03-31')
-- WHERE  fingerprint='F5136AE56909D48EBDE8EE2B0F230CD93AD91FF4'
-- AND scanner_country='DE';
--  scanner_country |               fingerprint                | ratio_avg | weight_bytes_avg | advertised_bw_avg | desc_bw_obs_last_avg | stream_bw_avg | advertised_bandwidth_avg | bandwidth_observed_avg
-- -----------------+------------------------------------------+-----------+------------------+-------------------+----------------------+---------------+--------------------------+------------------------
--  DE              | F5136AE56909D48EBDE8EE2B0F230CD93AD91FF4 |      0.83 |          2500000 |           3072000 |              3502740 |       1433634 |                  3072000 |
-- Depends on **materialized view** `bw_line`
CREATE OR REPLACE FUNCTION bw_avgs(
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE
)
RETURNS TABLE
(
    scanner_country TEXT,
    fingerprint TEXT,
    ratio_avg DOUBLE PRECISION,
    weight_bytes_avg BIGINT,
    advertised_bw_avg BIGINT,
    desc_bw_obs_last_avg BIGINT,
    stream_bw_avg BIGINT,
    advertised_bandwidth_avg BIGINT,
    bandwidth_observed_avg BIGINT
) AS
$body$
SELECT
    scanner_country,
    fingerprint,
    AVG(ratio) AS ratio_avg,
    AVG(weight_bytes) AS weight_bytes_avg,
    AVG(advertised_bw) AS advertised_bw_avg,
    AVG(desc_bw_obs_last) AS desc_bw_obs_last_avg,
    AVG(stream_bw) AS stream_bw_avg,
    -- Calculate averages with the last descriptors too
    AVG(advertised_bandwidth) as advertised_bandwidth_avg,
    AVG(bandwidth_observed) as bandwidth_observed_avg
FROM bw_line
WHERE published BETWEEN datestart AND dateend
AND running = true
-- When there aren't measurements yet, `stream_bw` is -1. In many cases this
-- will also filter out lines with `desc_bw_obs_last` -1 and ratio 0 (no
-- observed bandwidth available), but it won't filter out the cases in which
-- there are measurements, but ratio is 0 due to the sbws version not
-- publishing ratios (sbws < v1.8)
AND stream_bw > -1
GROUP BY fingerprint, scanner_country;
$body$
LANGUAGE sql;

-- SELECT * FROM lowest_ratio_highest_weight('2024-03-29', '2024-03-31', 1, 'DE');
--                fingerprint                | scanner_country | ratio_avg | weight_bytes_avg
-- ------------------------------------------+-----------------+-----------+------------------
--  28A148F6F39AC6C738B0D0F9FEAE24F43589E001 | DE              |      0.01 |             7000
CREATE OR REPLACE FUNCTION lowest_ratio_highest_weight (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    lim INT,
    VARIADIC scanner_country TEXT[]
)
RETURNS TABLE
(
    fingerprint TEXT,
    scanner_country TEXT,
    ratio_avg DOUBLE PRECISION,
    weight_bytes_avg BIGINT
) AS
$body$
    SELECT DISTINCT
        fingerprint,
        scanner_country,
        ratio_avg,
        weight_bytes_avg
    FROM bw_avgs(datestart, dateend)
    WHERE scanner_country = ANY ($4)
    AND ratio_avg > 0
    ORDER BY ratio_avg, weight_bytes_avg DESC
    LIMIT lim
$body$
LANGUAGE sql;

-- SELECT * FROM highest_weight_lowest_ratio('2024-03-29', '2024-03-31',10, 'DE');
--                fingerprint                | scanner_country | weight_bytes_avg |     ratio_avg
-- ------------------------------------------+-----------------+------------------+--------------------
--  3C89C80E2699FB6358BBB64FDC9547AFCB5C03F7 | DE              |        360000000 | 4.0649999999999995
--  9454F82D47EBA0CB0C2A4BFE7966181DBF108147 | DE              |        290000000 |              4.425
--  CBCC85F335E20705F791CFC8685951C90E24134D | DE              |        270000000 |              2.435
--  EFF782F13559E9303965F199418EC56DE699DC0C | DE              |        260000000 |               2.85
--  9971F51A3274758B5C59E1D6580ED2C13E13CBEC | DE              |        240000000 |               4.38
--  5E52BEA22130598F200D05C42BB66709C24E8F67 | DE              |        225000000 |               4.26
--  939126EA4D25CB212A79746C133194F8A24C435B | DE              |        220000000 |               4.38
--  9973E1E9730A58FDBA9E112D2B3342D2C0D921B5 | DE              |        213333333 |  4.023333333333333
--  7327876AE79C997DFE311A7B15B4FA875736BBD1 | DE              |        200000000 |               4.52
--  494E5000BA79F0078DFA05E610911C2AC525572F | DE              |        180000000 |               2.88
CREATE OR REPLACE FUNCTION highest_weight_lowest_ratio (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    lim INT,
    VARIADIC scanner_country TEXT[]
)
RETURNS TABLE
(
    fingerprint TEXT,
    scanner_country TEXT,
    weight_bytes_avg BIGINT,
    ratio_avg DOUBLE PRECISION
) AS
$body$
    SELECT DISTINCT
        fingerprint,
        scanner_country,
        weight_bytes_avg,
        ratio_avg
    FROM bw_avgs(datestart, dateend)
    WHERE scanner_country = ANY ($4)
    AND ratio_avg > 0
    ORDER BY weight_bytes_avg DESC, ratio_avg
    LIMIT lim
$body$
LANGUAGE sql;

-- SELECT * FROM weight_gt_stream('2024-03-29', '2024-03-31', 10, 'DE');
--                fingerprint                | scanner_country | weight_bytes_avg | stream_bw_avg |  weight_by_stream
-- ------------------------------------------+-----------------+------------------+---------------+--------------------
--  CBCC85F335E20705F791CFC8685951C90E24134D | DE              |        270000000 |       4216226 |  64.03831293673537
--  A7A6BAAE17FB162164327C34229E7D0D81D4DC82 | DE              |        150000000 |       2571638 |  58.32858279431242
--  EFF782F13559E9303965F199418EC56DE699DC0C | DE              |        260000000 |       4936941 | 52.664190234398184
--  67BE9A0658EF0106FCCB98FF1C68D6AAFFFC3CA9 | DE              |        140000000 |       2689318 |  52.05780796469588
--  3C89C80E2699FB6358BBB64FDC9547AFCB5C03F7 | DE              |        360000000 |       7039641 |   51.1389714333444
--  7B842FB48CFF19898C8336A11CAA3F425C90F9B5 | DE              |         81500000 |       1666357 |   48.9090873084219
--  6EDC0A79B083ECCE0D1B797B101ED88EB3ED90D1 | DE              |        170000000 |       3901570 |  43.57220298495221
--  C23A35AB5BF3052B020883EC8CE25AB4382CA745 | DE              |         50000000 |       1167240 |  42.83609197765669
--  2BCA0A8B5759DBD764BF9FA5D1B3AEE9D74D2B68 | DE              |        130000000 |       3127302 |  41.56937833314467
--  DF6F7908E7FDEEE5DE6DD9C92F7C9DFB20B0CE2A | DE              |        150000000 |       3614896 |  41.49496970313945
CREATE OR REPLACE FUNCTION weight_gt_stream (
    datestart TIMESTAMP WITHOUT TIME ZONE,
    dateend TIMESTAMP WITHOUT TIME ZONE,
    lim INT,
    VARIADIC scanner_country TEXT[]
)
RETURNS TABLE
(
    fingerprint TEXT,
    scanner_country TEXT,
    weight_bytes_avg BIGINT,
    stream_bw_avg BIGINT,
    weight_by_stream DOUBLE PRECISION
) AS
$body$
    SELECT DISTINCT
        fingerprint,
        scanner_country,
        avg(weight_bytes_avg) AS weight_bytes_avg_avg,
        avg(stream_bw_avg) AS stream_bw_avg_avg,
        avg(weight_bytes_avg) / avg(stream_bw_avg) AS weight_by_stream
    FROM bw_avgs(datestart, dateend)
    WHERE scanner_country = ANY ($4)
    GROUP BY fingerprint, scanner_country
    ORDER BY weight_by_stream DESC
    LIMIT lim
$body$
LANGUAGE sql;
