CREATE TABLE IF NOT EXISTS bridgedb_metrics(
  bridgedb_metrics_end          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  interval                      BIGINT                       NOT NULL,
  digest                        TEXT,
  version                       TEXT,
  PRIMARY KEY(digest)
);

CREATE TABLE IF NOT EXISTS bridgedb_metrics_count(
  digest                        TEXT                         NOT NULL,
  time                          TIMESTAMP WITHOUT TIME ZONE  NOT NULL,
  distribution                  TEXT,
  transport                     TEXT,
  country                       TEXT,
  status                        TEXT,
  tests                         TEXT,
  value                         BIGINT,
  metrics                       TEXT references bridgedb_metrics(digest),
  PRIMARY KEY(digest, time)
);
